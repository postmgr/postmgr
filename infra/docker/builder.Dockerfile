FROM rust:1.32-slim

WORKDIR /usr/src/postmgr
COPY . .

# Install postfix
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install pkg-config libssl-dev ca-certificates make \
  rsyslog telnet sqlite postfix \
  dovecot-imapd dovecot-pop3d dovecot-sqlite dovecot-pgsql
RUN ln -s /usr/sbin/postfix /usr/bin/postfix

RUN cargo install
