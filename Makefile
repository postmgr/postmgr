.PHONY: build clean clean-runtime run test watch \
				check-tool-cargo check-tool-cargo-watch \
				diesel-cli \
				builder-image builder-image-publish image publish registry-login get-version \
				container container-shell test-container

all: build

has-cargo:
	which cargo &&

VERSION=$(shell awk '/version\s+=\s+\"([0-9|\.]+)\"$$/{print $$3}' Cargo.toml)

CARGO := $(shell command -v cargo 2> /dev/null)
CARGO_WATCH := $(shell command -v cargo-watch 2> /dev/null)

# ENV = development | production
ENV=development

IMAGE_NAME:=postmgr
BUILDER_IMAGE_NAME:=postmgr/builder
REGISTRY_PATH=registry.gitlab.com/postmgr
LOCAL_TEST_IMAGE_NAME=postmgr-test
FQ_IMAGE_NAME=$(REGISTRY_PATH)/$(IMAGE_NAME):$(VERSION)
FQ_BUILDER_IMAGE_NAME=$(REGISTRY_PATH)/$(BUILDER_IMAGE_NAME):$(VERSION)

CONTAINER_NAME=postmgr

check-tool-cargo:
ifndef CARGO
	$(error "`cargo` is not available please install cargo (https://github.com/rust-lang/cargo/)")
endif

check-tool-cargo-watch:
ifndef CARGO_WATCH
	$(error "`cargo-watch` is not available please install cargo-watch (https://github.com/passcod/cargo-watch)")
endif

diesel-cli:
	cargo install diesel_cli --no-default-features --features sqlite

get-version:
	@echo -e ${VERSION}

dev-setup:
	cp .dev/git/hooks/pre-push .git/hooks && chmod +x .git/hooks/pre-push

clean: check-tool-cargo clean-runtime
	cargo clean

clean-runtime:
	rm -rf infra/runtime

build: check-tool-cargo
	cargo build

run:
	cargo run

test:
	cargo test

test-e2e:
	cargo test -- --ignored

watch: check-tool-cargo check-tool-cargo-watch
	cargo watch -x build

image:
	docker build \
	-f infra/docker/Dockerfile \
	-t $(FQ_IMAGE_NAME) \
	-t $(LOCAL_TEST_IMAGE_NAME) \
	.

builder-image:
	docker build -f infra/docker/builder.Dockerfile -t $(FQ_BUILDER_IMAGE_NAME) .

registry-login:
	docker login registry.gitlab.com

publish: registry-login
	docker push $(FQ_IMAGE_NAME)

builder-image-publish: registry-login
	docker push $(FQ_BUILDER_IMAGE_NAME)

container:
	docker stop $(CONTAINER_NAME) || true
	docker rm $(CONTAINER_NAME) || true
	docker run \
	--detach \
	-p 2525:25 \
	-p 5875:587 \
	-e CONFIG_PATH=/usr/src/postmgr/infra/conf/${ENV}.toml \
	--name $(CONTAINER_NAME) \
	$(FQ_IMAGE_NAME)

container-shell:
	docker run -it \
	-p 2525:25 \
	-p 5875:587 \
	-e CONFIG_PATH=/usr/src/postmgr/infra/conf/${ENV}.toml \
	$(FQ_IMAGE_NAME) \
	/bin/bash

test-container: container
