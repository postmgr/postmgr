# NOTE: This project isn't being actively worked on, partly because there are better options out there

- https://maddy.email
- https://blitiri.com.ar/p/chasquid/
- https://haraka.github.io/

I may pick up `postmgr` up again in the future but these other projects suffer much less of the `postfix` baggage and are robust and configurable. It may not make sense to do lots of work to make adapt `postfix` if the above options fit your usecase (assuming your use case doesn't depend on deep features of `postfix`).

# postmgr #

`postmgr` is a tool for managing instances of [Postfix][postfix]. `postmgr` aims to provide:

- Easier management, administration, and configuration for Postfix
- An easy to use web interface for mailbox and other management tasks

# Quick Start #

**`postmgr` is meant to be run in a properly sandboxed/isolated (i.e. containerized) environment** -- it attempts to overwrite files in directories like `/etc/postfix` due to `postfix`'s shoddy support for determining `config_directory` ("-c" and/or `MAIL_CONFIG` do not completely ensure alternate configuration directories) consistently for *all* related processes.

The quickest way to get started with `postmgr` is to use one of our prebuilt containers. If you want to build your own container or other deployment method with `postmgr`, you can obtain the `postmgr` binary, and as long as you tell `postmgr` where it can find the other binaries it needs, it will manage them.

The [`postmgr` container][gitlab-docker-repo] is the primary container of this project.

You **should** be able to download the single container, connect it to your container orchestration process of choice, and have a fully functional, mostly secure, manageable Postfix installation. If this is ever not-true, [file a bug][gitlab-issues].

# Why postmgr? #

While some might [advise against running your own mail server][do-dont-run-your-own-mailserver], for relatively simple applications that need to send mail, I often find it's easiest to set up a small Postfix instance to handle things like registration emails and the like. While the documentation is extensive, and many resources abound, managing Postfix can be a very difficult endeavor, with misleading/overloaded terminology, and seemingly cryptic settings.

Projects like [Mailu][mailu], [mailcow][mailcow], [mail-in-a-box][mail-in-a-box] make it much easier, but they leave much to be desired when running in a managed-container context like [Kubernetes][k8s]. At the time of this project's creation, Mailu and mailcow both offer what I consider to be inadequate support for both running in k8s and general maintenance ability/dependability/ease of deployment, and while *more tight coupling* is usually never the right answer, I think it could help with.

One of the express goals of this project is to make management, configuration, and administration of Postfix easier.

# Architecture #

The basic architecture of `postmgr` is pretty simple:

```
+------------------------------+
|                              |
|     Postfix Component        |
|                              |         +-----------------+
|                              |         |                 |
|  +----------+   +----------+ +<--------+    Dovecot      |
|  |          |   |          | |         |                 |
|  | postfix  |   |MailboxDB | +-------->+  +-----------+  |
|  | process  |   |          | |         |  |           |  |
|  |          |   |          | |         |  |dovecot    |  |
|  +----------+   +----------+ |         |  |process    |  |
|                              |         |  +-----------+  |
+----------+-------+-----------+         +-----------------+
           |       ^
           |       |
           |       |
           |       |
           v       |
+----------+-------+-----------+
|                              |
|                              |
|     Web Admin Component      |
|                              |
|                              |
+------------------------------+
```

There are three major components:

- Postfix (`src/components/postfix.rs`) which controls and manipulates a child `postfix` process
- Web Admin (`src/components/web_admin.rs`) which exposes an administration interface, sending messages to the Postfix component
- Dovecot (`src/components/dovecot.rs`) which controls and manipulates a child `dovecot` process

`postmgr` listens on the ports you would expect Postfix to listen on (25, 587, etc), plus a few more for value-added features (like Dovecot).

For more more information on various bits of the architecture:

- IMAP (via dovecot): `docs/imap.md`

# Developing/debugging `postmgr` #

If you'd like to contribute to `postmgr`, use the following steps:

1. Install [the rust toolchain][rust-toolchain]
2. `git clone` this repository
3. `make diesel-cli` to install diesel CLI if it's not already installed
4. `make dev-setup` to get set up
5. Make your changes
6. Run the test suite (`make test`, `make test-e2e`) and make sure they pass (the more tests you've added, the more likely your MR will succeed)
7. Make a merge request against this repository

For more information on testing, see `docs/testing.md`

[postfix]: http://www.postfix.org/
[do-dont-run-your-own-mailserver]: https://www.digitalocean.com/community/tutorials/why-you-may-not-want-to-run-your-own-mail-server
[mailu]: https://github.com/Mailu/Mailu/
[mailcow]: https://github.com/mailcow/mailcow-dockerized
[mail-in-a-box]: https://mailinabox.email/
[k8s]: https://kubernetes.io/
[gitlab-docker-repo]: https://gitlab.com/postmgr/postmgr/repo
[gitlab-issues]: https://gitlab.com/postmgr/postmgr/issues
[rust-toolchain]: https://www.rust-lang.org/en-US/install.html
