requirejs.config({
  //By default load any module IDs from js/lib
  baseUrl: '',
  //except, if the module ID starts with "app",
  //load it from the js/app directory. paths
  //config is relative to the baseUrl, and
  //never includes a ".js" extension since
  //the paths config could be for a directory.
  paths: {
    app: 'js/',
    vendor: 'js/vendor',
    mithril: 'js/vendor/mithril.min',
    lodash: 'js/vendor/lodash.min',
  },
  shim: {
    mithril: { exports: 'm' },
    lodash: { exports: '_' }
  },
  map: {
    '*': {
      'css': 'vendor/require-css.min'
    }
  }
});

require([
  "mithril",
  "app/components/index-page/component",
  "css!app/main.css"
], function(m, IndexPage) {

  // Routes
  m.route(document.body, "/home", {
    "/home": IndexPage
  });

});
