/* global define */

define([
  "mithril",
  "app/services/alert",
  "app/constants",
], function(
  mithril,
  AlertSvc,
  C,
) {

  const VALID_COMPONENTS = [C.PostfixComponent, C.AdminComponent];

  class ComponentService {

    constructor() {
      this.components = {};
      this.components[C.PostfixComponent] = {};
      this.components[C.AdminComponent] = {};
    }

    getComponentStatus(name) {
      if (!VALID_COMPONENTS.includes(name)) {
        return Promise.reject(new Error("Invalid component"));
      }

      return m
        .request({
          method: "GET",
          url: C.URLS.api.v1.components.status(name)
        })
        .then(status => this.components[name].status = status)
        .catch(err => {
          AlertSvc.addAppWideAlert({message: `Failed to retrieve [${name}] service status`, type: "error", err});
        });
    };
  }

  return new ComponentService();
});
