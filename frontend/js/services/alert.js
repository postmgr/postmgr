/* global define */

define(function() {

  const APP_ALERT_NAMESPACE = "app";

  class AlertService {

    constructor() {
      this.alerts = {};
      this.alerts[APP_ALERT_NAMESPACE] = [];
    }

    /**
     * Add a page level alert
     *
     * @param {Alert} alert
     * @returns A promise that resolves when the alert has been added
     */
    addAppWideAlert(alert) { return this.addAlert(APP_ALERT_NAMESPACE, alert); }

    /**
     * Add an alert to a given namespace
     *
     * @param {string} ns - namespace
     * @param {Alert} alert - the Alert to add
     * @returns a Promise that resolves whent he alert has been added
     */
    addAlert(ns, alert) {
      if (!(ns in this.alerts)) {
        return Promise.reject(new Error("Unrecognized ns"));
      }

      this.alerts[ns].push(alert);
      return Promise.resolve();
    }

    /**
     * Add a namespace to the alerts service
     *
     * @param {string} ns - namespace name
     */
    registerNamespace(ns) {
      if (ns in this.alerts) { return; }

      this.alerts[ns] = [];
    }

    /**
     * Retrieve the alerts for a given namespace
     *
     * @param {string} ns
     * @returns a Promise that resolves to a list of alerts for the given namespace
     */
    getAlertsForNamespace(ns) {
      if (!(ns in this.alerts)) { return Promise.reject(new Error("Unrecognized namespace")); }
      return Promise.resolve(this.alerts[ns]);
    }

  }

  return new AlertService();
});
