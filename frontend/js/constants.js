/* global define */

define(function() {

  return {
    PostfixComponent: "postfix",
    AdminComponent: "admin",

    icons: {
      components: {
        postfix: "fa-envelope",
        admin: "fa-toolbox"
      }
    },

    ComponentStatus: {
      Running: "running",
      Stopped: "stopped"
    },

    URLS: {
      api: {
        v1: {
          components: {
            status: (name) => `/api/v1/components/${name}/status`
          }
        }
      }
    }
  };

});
