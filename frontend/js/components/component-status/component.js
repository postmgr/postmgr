define([
  "lodash",
  "mithril",
  "app/constants",
], function(
  _,
  m,
  C,
) {

  const statusTextClasses = (status) => {
    if (status === C.ComponentStatus.Running) {
      return "bg-emerald fg-white";
    }

    return "bg-concrete";
  };

  class ComponentStatus {
    constructor(vnode) {
      this.componentName = vnode.attrs.name || "Component";
      this.status = vnode.attrs.status || "unknown";
      this.iconCls = _.get(C, `icons.components.${this.componentName}`) || "fa-question";
    }

    view(vnode) {
      return m("div", [
        m("i", {class: `fa ${this.iconCls} lg-text xs-margin-right`}),
        m("span", {class: "lg-text xs-margin-right"}, vnode.attrs.name),
        m("span", {class: `valign-super tag ${statusTextClasses(vnode.attrs.status)}`}, vnode.attrs.status)
      ]);
    }
  }

  return ComponentStatus;
});
