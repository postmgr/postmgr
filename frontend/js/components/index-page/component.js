/* global define */
define([
  "lodash",
  "mithril",
  "app/services/component",
  "app/components/page-header/component",
  "app/components/component-status/component",
  "app/constants",
  "css!./style.css"
], function(
  _,
  m,
  ComponentSvc,
  PageHeader,
  ComponentStatus,
  C,
) {

  class IndexPage {
    oninit() {
      ComponentSvc.getComponentStatus(C.PostfixComponent);
      ComponentSvc.getComponentStatus(C.AdminComponent);
    }

    view() {
      return m("div", {class: "index-page-component bg-wet-asphalt fg-white"}, [
        m(PageHeader, {subtitle: "Component Status"}),
        m("div", {class: "page centered-text"}, [

          // Postfix component
          m(ComponentStatus, {
            class: "xs-margin-bottom",
            name: C.PostfixComponent,
            status: _.get(ComponentSvc.components[C.PostfixComponent], 'status.state')
          }),

          m("br"),

          // Admin component status
          m(ComponentStatus, {
            class: "xs-margin-bottom",
            name: C.AdminComponent,
            status: _.get(ComponentSvc.components[C.AdminComponent], 'status.state')
          })
        ])
      ]);
    }
  }

  return IndexPage;
});
