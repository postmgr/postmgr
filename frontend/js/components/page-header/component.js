define([
  "mithril",
  "app/constants",
], function(
  m,
  Constants,
  ComponentSvc
) {

  class PageHeader {
    constructor(vnode) {
      this.title = vnode.attrs.title || "postmgr";
      this.subtitle = vnode.attrs.subtitle || "";
    }

    view() {
      return m("section", {class: "page-title-container bg-wet-asphalt centered-text"}, [
        m("h1", {class: "page-title"}, this.title),
        m("h2", {class: "page-sub-title"}, this.subtitle)
      ]);
    }
  }

  return PageHeader;
});
