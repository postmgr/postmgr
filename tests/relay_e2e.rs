extern crate reqwest;

use std::net::TcpStream;
use std::io::{BufReader};

mod common;
use crate::common::DockerizedPostmgr;
use crate::common::fixtures;
use common::{
    DOMAIN_NAME,
    POSTMASTER_AT_LOCALHOST,

    read_server_hello,
    send_ehlo,
    send_test_message,
    send_rcpt_to,
    send_mail_from,
    plain_login_with_username_password,
};

#[test]
#[ignore]
/// Test basic SMTP connectivity
pub fn test_connect() {
    // Start postmgr in a docker container
    let postmgr = DockerizedPostmgr::new().start();

    // Connect to the postmgr over port 25
    let stream = TcpStream::connect(postmgr.get_smtp_address()).unwrap();
    let mut reader = BufReader::new(stream.try_clone().unwrap());

    // Read the response (connection should send SMTP response)
    read_server_hello(&mut reader);
}

#[test]
#[ignore]
/// Test basic SMTP email sending
pub fn test_email_send() {
    // Start postmgr in a docker container
    let postmgr = DockerizedPostmgr::new().start();

    // Connect to the postmgr over (unprotected) port 25
    let (stream, mut reader) = postmgr.connect_tcp().unwrap();

    // Ensure EHLO was received
    read_server_hello(&mut reader);

    // Send a client EHLO
    let (stream, reader) = send_ehlo(DOMAIN_NAME, stream, reader);

    // MAIL FROM
    let (stream, reader) = send_mail_from(POSTMASTER_AT_LOCALHOST, stream, reader);
    // RCPT TO
    let (stream, reader) = send_rcpt_to(POSTMASTER_AT_LOCALHOST, stream, reader);
    // send message contents
    send_test_message("test email", "this is a test", stream, reader);
}

#[test]
#[ignore]
/// Test user creation & interaction over basic SMTP relay
pub fn test_user_create_and_login() {
    // Start postmgr in a docker container
    let postmgr = DockerizedPostmgr::new().start();

    // TODO: Create a new user through the API
    let base_url = postmgr.get_http_address();
    let client = reqwest::Client::new();

    // Add a new user
    let url = format!("http://{}/{}", &base_url, "api/v1/users");
    let test_user = fixtures::test_user();
    let resp = client.post(url.as_str())
        .json(&test_user)
        .send()
        .expect("failed to POST new user");

    assert_eq!(resp.status(), 201, "new user creation worked");

    // Connect to the postmgr over (unprotected) port 25
    let (stream, mut reader) = postmgr.connect_tcp().unwrap();

    // Ensure EHLO was received
    read_server_hello(&mut reader);

    // Send a client EHLO
    let (stream, reader) = send_ehlo(DOMAIN_NAME, stream, reader);

    // Login with username/pw for the new user
    let user = fixtures::test_user();
    plain_login_with_username_password(
        &format!("{}@{}", &user.username, &user.domain),
        &user.password,
        stream,
        reader,
    );
}

// TODO: test sending email between two local accounts
// TODO: test user delete (after deleting through the API a user should be unable to log in)
