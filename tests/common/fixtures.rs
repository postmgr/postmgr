use common::postmgr::models::user::MailboxUser;

pub fn test_user() ->  MailboxUser {
    MailboxUser {
        username: String::from("test"),
        domain: String::from("localhost"),
        home: String::from("/var/mail/test"),
        password: String::from("test"),
        quota_gb: 5
    }
}
