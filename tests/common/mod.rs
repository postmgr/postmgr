extern crate tempfile;
extern crate rand;
extern crate mail;
extern crate new_tokio_smtp;
extern crate postmgr;
extern crate base64;

pub mod fixtures;

use std::process::{Command, Stdio};
use std::net::{TcpListener, Ipv4Addr};
use std::time::Duration;
use std::thread;
use std::io::{Error as IOError};
use std::net::TcpStream;
use std::io::{Write, BufRead, BufReader};

use self::rand::{thread_rng, Rng};
use self::rand::distributions::Alphanumeric;
use self::rand::seq::sample_iter;

pub const DOMAIN_NAME: &'static str = "localhost";
pub const POSTMASTER_AT_LOCALHOST: &'static str = "postmaster@localhost";
pub const CR_LF_END: &'static str = "\r\n.\r\n";
pub const AUTH_SUCCESS_CODE: &'static str = "235";
pub const SMTP_SUCCESS_PARTIAL_CODE: &'static str = "250-";
pub const SMTP_SUCCESS_FINISH_CODE: &'static str = "250 ";
pub const SMTP_AUTH_CHALLENGE_CODE: &'static str = "334";
// pub const AUTH_SUCCESS_MSG: &'static str = "Authentication succeeded";
// pub const SMTP_B64_USERNAME_PROMPT: &'static str = "VXNlcm5hbWU6";
// pub const SMTP_B64_PASSWORD_PROMPT: &'static str = "UGFzc3dvcmQ6";

// Get available port
// https://elliotekj.com/2017/07/25/find-available-tcp-port-rust/
const TMP_PORT_RANGE_START: u16 = 8000;
const TMP_PORT_RANGE_END: u16 = 9000;

const POSTMGR_TEST_IMAGE_NAME: &'static str = "postmgr-test";
const DOCKER_BINARY: &'static str = "docker";
const POST_STARTUP_WAIT: Duration = Duration::from_secs(3);


fn get_available_port() -> Option<u16> {
    let mut rng = thread_rng();
    let mut available = false;
    let mut port = 0;

    while !available {
        port = sample_iter(&mut rng, TMP_PORT_RANGE_START..TMP_PORT_RANGE_END, 1).unwrap()[0];
        available = port_is_available(port);
    }

    Some(port)
}

fn port_is_available(port: u16) -> bool {
    TcpListener::bind(("127.0.0.1", port)).is_ok()
}

pub struct DockerizedPostmgr {
    id: String,
    pub container_name: String,

    // connection details
    host: Ipv4Addr,
    smtp_port: u16,
    submission_port: u16,
    http_port: u16,

    // username-related information
    pub domain_name: String,
}

impl DockerizedPostmgr {
    pub fn new() -> DockerizedPostmgr {
        let host = Ipv4Addr::LOCALHOST;
        let smtp_port = get_available_port().unwrap();
        let submission_port = get_available_port().unwrap();
        let http_port = get_available_port().unwrap();
        let domain_name = String::from(DOMAIN_NAME);
        let id = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(5)
            .collect::<String>();
        let container_name = format!("postmgr-test-{}", &id);

        DockerizedPostmgr{
            id,
            container_name,

            host,
            smtp_port,
            submission_port,
            http_port,

            domain_name,

        }
    }

    pub fn start(self) -> Self {
        // Create the docker child process
        let smtp_port_assignment = format!("{}:25", self.smtp_port);
        let submission_port_assignment = format!("{}:587", self.submission_port);
        let http_port_assignment = format!("{}:8080", self.http_port);

        let process = Command::new(DOCKER_BINARY)
            .arg("run")
            .arg("--detach")
            .arg("-p")
            .arg(smtp_port_assignment)
            .arg("-p")
            .arg(submission_port_assignment)
            .arg("-p")
            .arg(http_port_assignment)
            .arg("--name")
            .arg(&self.container_name)
            .arg(POSTMGR_TEST_IMAGE_NAME)
            .stdin(Stdio::null())
            .stdout(Stdio::null()) // NOTE: comment this out to get docker run output
            .spawn()
            .expect("failed to start docker run command");

        let output = process
            .wait_with_output()
            .expect("docker run command failed");
        if !output.status.success() { panic!("docker run returned a non-success exit code"); }

        println!(
            "created docker container [{}], waiting {} seconds for startup...",
            &self.container_name,
            POST_STARTUP_WAIT.as_secs(),
        );

        // Wait a little while for the process to come up
        // FIXME: This is fragile, would be much better to wait for some specific output from stdout
        thread::sleep(POST_STARTUP_WAIT);

        self
    }

    /// Connect to the instance over TCP
    pub fn connect_tcp(&self) -> Result<(TcpStream, BufReader<TcpStream>), IOError> {
        let stream = TcpStream::connect(self.get_smtp_address())?;
        let reader = BufReader::new(stream.try_clone()?);

        Ok((stream, reader))
    }

    pub fn get_smtp_address(&self) -> String {
        format!("{}:{}", self.host, self.smtp_port)
    }

    // pub fn get_submission_address(&self) -> String {
    //     format!("{}:{}", self.host, self.smtp_port)
    // }

    pub fn get_http_address(&self) -> String {
        format!("{}:{}", self.host, self.http_port)
    }
}

impl Drop for DockerizedPostmgr {
    fn drop(&mut self) {
        // DEBUG: comment the lines below to leave dangling containers after tests

        // Kill the docker process
        Command::new(DOCKER_BINARY)
            .arg("kill")
            .arg(&self.container_name)
            .stdin(Stdio::null())
            .stdout(Stdio::null()) // NOTE: comment this out to get docker kill output
            .output()
            .expect("failed to start docker kill command");
    }
}

/// Read the server hello
pub fn read_server_hello(reader: &mut BufReader<TcpStream>) -> &mut BufReader<TcpStream> {
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    println!("EHLO response: [{}]", line.trim());

    assert!(line.contains("220"), "EHLO response contained '220'");
    assert!(line.contains("Postfix"), "EHLO response contained 'Postfix'");
    reader
}

/// Send MAIL FROM and verify response
pub fn send_mail_from(
    address: &str,
    mut stream: TcpStream,
    mut reader: BufReader<TcpStream>
) -> (TcpStream, BufReader<TcpStream>) {
    let cmd = format!("MAIL FROM: <{}>\n", address);
    stream.write(cmd.as_bytes()).unwrap();
    println!("SENT [{}]", cmd);

    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    println!("MAIL FROM response: [{}]", line.trim());

    assert!(line.contains("250"), "MAIL FROM response succeeded");
    (stream, reader)
}

/// Send RCPT TO and verify response
pub fn send_rcpt_to(
    recipient: &str,
    mut stream: TcpStream,
    mut reader: BufReader<TcpStream>
) -> (TcpStream, BufReader<TcpStream>) {
    let cmd = format!("RCPT TO: <{}>\n", recipient);
    stream.write(cmd.as_bytes()).unwrap();
    println!("SENT [{}]", cmd);

    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    println!("RCPT TO response: [{}]", line.trim());

    assert!(line.contains("250"), "RCPT TO response succeeded");
    (stream, reader)
}

/// Send a test message with a given subject/body
pub fn send_test_message(
    subject: &str,
    body: &str,
    mut stream: TcpStream,
    mut reader: BufReader<TcpStream>
) -> (TcpStream, BufReader<TcpStream>) {
    let mut line = String::new();

    // Send data start
    let cmd = format!("DATA\n");
    stream.write(cmd.as_bytes()).unwrap();
    reader.read_line(&mut line).unwrap();
    // Postfix sends 354 here to indicate how to end the data
    println!("DATA response: [{}]", line.trim());
    assert!(line.contains("354") && line.contains("End data with"), "DATA command succeeded");
    line.clear();

    // Send the test
    let cmd = format!("subject: {}\n{}\n{}", subject, body, CR_LF_END);
    stream.write(cmd.as_bytes()).unwrap();
    reader.read_line(&mut line).unwrap();
    println!("Message send response: [{}]", line.trim());

    assert!(line.contains("250") && line.contains("Ok:"), "Message successfully queued");
    (stream, reader)
}

/// Send EHLO as a client
pub fn send_ehlo(
    hostname: &str,
    mut stream: TcpStream,
    mut reader: BufReader<TcpStream>,
) -> (TcpStream, BufReader<TcpStream>) {
    // Send EHLO
    let cmd = format!("EHLO {}\n", hostname);
    stream.write(cmd.as_bytes()).unwrap();

    // Read start of 250-/250 responses
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    assert!(line.contains(SMTP_SUCCESS_PARTIAL_CODE), "EHLO success response started");

    // Read until the 250 finish code (last line of success output) shows up
    while !line.contains(SMTP_SUCCESS_FINISH_CODE) {
        line.clear();
        reader.read_line(&mut line).unwrap();
    }

    assert!(line.contains(SMTP_SUCCESS_FINISH_CODE), "EHLO success response finished");

    (stream, reader)
}

/// Perform login with a given username and password
pub fn plain_login_with_username_password(
    username: &str,
    password: &str,
    mut stream: TcpStream,
    mut reader: BufReader<TcpStream>
) -> (TcpStream, BufReader<TcpStream>) {
    let mut line = String::new();

    // Send AUTH PLAIN
    let cmd = format!("AUTH PLAIN\n");
    stream.write(cmd.as_bytes()).unwrap();

    // Read response
    reader.read_line(&mut line).unwrap();
    println!("AUTH PLAIN response: [{}]", line.trim());

    // Postfix sends 334 to start asking for username
    assert!(
        line.contains(SMTP_AUTH_CHALLENGE_CODE),
        "AUTH PLAIN command succeeded"
    );
    line.clear();

    // Send username and password
    let creds = format!("\0{}\0{}", &username, &password);
    let cmd = format!("{}\n", base64::encode(&creds));
    stream.write(cmd.as_bytes()).unwrap();
    reader.read_line(&mut line).unwrap();
    println!("response to username & password send: [{}]", line.trim());
    assert!(
        line.contains(AUTH_SUCCESS_CODE)
        "credentials sent successfully"
    );
    line.clear();

    (stream, reader)
}
