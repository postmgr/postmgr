# IMAP #

## Supported clients ##

Curently [`dovecot`](https://wiki.dovecot.org) is the only supported IMAP client.

The `Dovecot` component (`src/components/dovecot.rs`) manages a child `dovecot` process (which is started in non-daemonized mode for better control), generating and placing configuration as necessary.

## Suported SMTP authentication method ##

Currently the following authentication methods are supported via `dovecot`:

- `AUTH PLAIN`
