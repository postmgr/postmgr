# Hacks #

This document details hacks/hacky solutions that have transpired in the project and the reasoning behind them.

## String based UUIDs over numeric IDs ##

Early in the writing of this project, [diesel][diesel] did not support the `SupportsReturningClasuse` implemented for `sqlite`. This meant that there wasn't easy support for handling the fields reutrned by the SQLite query.

Up until that point in time, both numberic and string-based UUID use was planned, but at that point the decision to go all in on UUIDs was made -- this way they can be generated at the client before insertion is attempted, be known and returned (if the statement was successful).

**Pros**
- Consistent and singular identification for all models
- Obscures DB information (like # of users)

**Cons**
- Relatively inefficient when compared to integers
- Longer URLs
