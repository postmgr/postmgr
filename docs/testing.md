# Testing #

## Debugging E2E tests ##

To run the E2E tests without output suppression:

```
$ cargo test -- --ignored --nocapture
```
