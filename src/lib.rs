pub mod config;
pub mod components;
pub mod models;


#[allow(unused_imports)] // rust incorrectly warns that askama is unused
#[macro_use] extern crate askama;

#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;

extern crate actix_web;
extern crate chrono;
extern crate clap;
extern crate fern;
extern crate futures;
extern crate rusqlite;
extern crate serde_json;
extern crate simple_signal;
extern crate toml;
extern crate uuid;

use std::env;
use std::process;
use clap::{App, Arg, SubCommand, ArgMatches};
use components::Error;
use std::path::{Path, PathBuf};
use std::fs::{canonicalize, create_dir_all};
use std::env::current_dir;

const CONFIG_PATH_ENV_VAR: &str = "CONFIG_PATH";
const DEFAULT_CFG_PATH: &str = "./infra/conf/development.toml";

use config::{gather_config, AppCfg};

// Gather tha app config from the environment (including related files
pub fn generate_app_config(cfg_path_override: Option<&str>) -> AppCfg {
    // Retrieve configuration file path
    let cfg_path = cfg_path_override
        .map(String::from)
        .unwrap_or(env::var(CONFIG_PATH_ENV_VAR)
                   .unwrap_or(String::from(DEFAULT_CFG_PATH)));

    // Gather configuration
    let maybe_app_cfg = gather_config(cfg_path.clone());
    if maybe_app_cfg.is_err() {
        debug!("Error: {}", maybe_app_cfg.unwrap_err());
        process::exit(1);
    }

    info!("app config loaded from file: [{}]", cfg_path);
    maybe_app_cfg.unwrap()
}

// Setup/Initialize fern based logging
pub fn setup_logging() {
    // Configure logger
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
    // Add blanket level filter -
        .level(log::LevelFilter::Debug)
    // - and per-module overrides
        .level_for("tokio", log::LevelFilter::Warn)
        .level_for("tokio_reactor", log::LevelFilter::Warn)
        .level_for("tokio_core", log::LevelFilter::Warn)
    // Output to stdout, files, and other Dispatch configurations
        .chain(std::io::stdout())
    // Apply globally
        .apply()
        .unwrap()
}

// Use clap to parse arguments
pub fn parse_arguments<'a>() -> ArgMatches<'a> {
    // Parse CLI arguments
    App::new("postmgr")
        .version("0.0.1")
        .about("Manage and administer postfix instances with ease")
        .arg(Arg::with_name("config")
             .short("c")
             .long("config")
             .value_name("FILE")
             .help("Custom config file (TOML)")
             .takes_value(true))
    // `postmgr serve`
        .subcommand(SubCommand::with_name("server").about("start the postmgr components"))
    // `postmgr postfix ...`
        .subcommand(SubCommand::with_name("postfix")
                    .about("postfix component related utilities")
                    // `postmgr postfix db-init ...`
                    .subcommand(SubCommand::with_name("db-init")
                                .about("connect to and initialize the configured database"))
                    // `postmgr postfix config ...`
                    .subcommand(SubCommand::with_name("config")
                                .about("manage configuration of postfix component")
                                // `postmgr postfix config generate ...`
                                .subcommand(SubCommand::with_name("generate-files")
                                            .about("generate configuration files"))
                                .subcommand(SubCommand::with_name("show")
                                            .about("show app configuration")))
                    // `postmgr postfix mailbox-user-add ...`
                    .subcommand(SubCommand::with_name("mailbox-user-add")
                                .about("add a mailbox user")
                                .arg(Arg::with_name("username")
                                     .required(true)
                                     .short("e")
                                     .long("username")
                                     .value_name("USERNAME")
                                     .takes_value(true))
                                .arg(Arg::with_name("domain")
                                     .required(true)
                                     .short("e")
                                     .long("domain")
                                     .value_name("DOMAIN")
                                     .takes_value(true))
                                .arg(Arg::with_name("password")
                                     .required(true)
                                     .short("p")
                                     .long("password")
                                     .value_name("PASSWORD")
                                     .takes_value(true))
                                .arg(Arg::with_name("mailbox_quota_gb")
                                     .short("q")
                                     .long("mailbox-quota-gb")
                                     .value_name("NUM_BYTES")
                                     .takes_value(true)))
                    // `postmgr postfix mailbox-user-remove ...`
                    .subcommand(SubCommand::with_name("mailbox-user-remove")
                                .about("remove a mailbox user")
                                .arg(Arg::with_name("username")
                                     .required(true)
                                     .short("e")
                                     .long("username")
                                     .value_name("USERNAME")
                                     .takes_value(true))))
        .get_matches()
}

/// Get the absolute path relative to the binary for a given path (if it's not absolute already)
pub fn make_absolute_path(path: PathBuf) -> Result<String, Error> {
    let abs_path;

    if path.exists() && path.is_absolute() {
        abs_path = path;
    } else if path.exists() {
        // this is necessary because canonicalize doesn't work on files that *DON'T YET EXIST*
        abs_path = canonicalize(path)?;
    } else {
        let mut base_path = current_dir()?;
        base_path.push(path);
        abs_path = base_path;
    }

    abs_path
        .to_str()
        .map(String::from)
        .ok_or(Error::NotSupported)
}

pub fn make_absolute_path_from_str(path: &String) -> Result<String, Error> {
    make_absolute_path(PathBuf::from(path))
}

/// Ensure a given directory exists (creating if it necessary), returning the absolute path to the directory)
pub fn ensure_directory(path: &str) -> Result<String, Error> {
    // Generate the directories the files are going to go into
    let dir = Path::new(path);
    debug!("ensuring configuration output path [{:?}] exists...", dir.to_str());

    // Ensure config directory exists and is a proper directory
    if !dir.exists() { create_dir_all(&dir)?; }
    if dir.exists() && !dir.is_dir() {
        let msg = format!("Provided directory [{:?}] is invalid", dir.to_str());
        return Err(Error::InvalidRuntimeEnvironment(msg));
    }

    make_absolute_path(dir.to_path_buf())
}
