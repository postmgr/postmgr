#[macro_use] extern crate log;
extern crate fern;
extern crate postmgr;

use std::thread;
use postmgr::components::db::{MailboxDB, Connectable, SupportsDovecotAuth};
use postmgr::components::web_admin::WebAdmin;
use postmgr::components::postfix::Postfix;
use postmgr::components::dovecot::Dovecot;
use postmgr::components::{Component, HasReqRespCommandBus, FileConfigurable, Configurable};
use postmgr::models::user::MailboxUser;
use postmgr::{setup_logging, generate_app_config, parse_arguments};
use std::process;

fn main() {
    setup_logging();
    let arg_matches = parse_arguments();
    let app_cfg = generate_app_config(arg_matches.value_of("config"));

    // Handle postfix subcommands
    match arg_matches.subcommand() {

        // handle `postmgr server ...`
        ("server", Some(_)) => {

            // Create Postfix component
            let mut postfix = Postfix::new(app_cfg.postfix.clone(), None).expect("postfix component init failed");

            // Create Dovecot config using the db from postfix
            // Use userdb/passdb settings from postfix's internal DB if no explicit settings provided
            let mut dovecot_cfg = app_cfg.dovecot.clone();
            if dovecot_cfg.userdb_settings.is_none() {
                if let Ok(s) = postfix.db.dovecot_userdb_settings() { dovecot_cfg.userdb_settings = Some(s);}
            }
            if dovecot_cfg.passdb_settings.is_none() {
                if let Ok(s) = postfix.db.dovecot_passdb_settings() { dovecot_cfg.passdb_settings = Some(s);}
            }

            // Create Dovecot component
            let mut dovecot = Dovecot::new(dovecot_cfg).expect("dovecot component init failed");

            // Create Web Admin component
            let postfix_tx = postfix.get_cmd_bus_tx().expect("failed to create WebAdmin -> Postfix communication channel");
            let dovecot_tx = dovecot.get_cmd_bus_tx().expect("failed to create WebAdmin -> Dovecot communication channel");
            let mut admin = WebAdmin::new(
                app_cfg.web_admin.clone(),
                postfix_tx,
                dovecot_tx,
            ).expect("admin component init failed");

            // Update config for postfix
            if let Err(postfix_config_err) = postfix.update_config(None) {
                error!("failed to setup postfix config: {}", postfix_config_err);
                process::exit(1);
            }

            // Update config for dovecot
            if let Err(dovecot_config_err) = dovecot.update_config(None) {
                error!("failed to setup dovecot config: {}", dovecot_config_err);
                process::exit(1);
            }

            // Start dovecot
            let dovecot_thread = thread::spawn(move|| { dovecot.start() });
            debug!("successfully started threaded dovecot component");

            // Start postfix
            let postfix_thread = thread::spawn(move|| { postfix.start() });
            debug!("successfully started threaded postfix component");

            // Start web admin
            let admin_thread = thread::spawn(move|| { admin.start() });
            debug!("successfully started threaded admin component");

            let _ = dovecot_thread.join();
            let _ = postfix_thread.join();
            let _ = admin_thread.join();
        },

        // handle `postmgr postfix ...`
        ("postfix", Some(postfix_m)) => {
            match postfix_m.subcommand() {

                // handle `postmgr postfix db-init ...`
                ("db-init", Some(_)) => {

                    let mut postfix = Postfix::new(app_cfg.postfix.clone(), None).expect("postfix component init failed");
                    let _ = postfix.db.connect();

                    info!("successfully connected to & initialized postfix DB");

                }, // END `postmgr postfix db-init ...`

                // handle `postmgr postfix config ...`
                ("config", Some(postfix_config_m)) => {

                    match postfix_config_m.subcommand() {

                        // handle `postmgr postfix config show`
                        ("show", Some(_)) => { info!("{:#?}", app_cfg); },

                        // handle `postmgr postfix config generate`
                        ("generate-files", Some(_)) => {

                            let mut postfix = Postfix::new(app_cfg.postfix.clone(), None).expect("postfix component init failed");

                            info!("Writing configuration to directory [{}]", postfix.config_dir_path());
                            if let Err(e) = postfix.generate_config_in_dir(None) {
                                error!("Failed to generate config: {}", e)
                            }

                        },

                        _ => panic!("Unrecognized postfix subcommand!"),
                    }

                }, // END `postmgr postfix config ...`

                // handle `postmgr postfix mailbox-user-add ...`
                ("mailbox-user-add", Some(postfix_add_mailbox_user_m)) => {

                    // Create components
                    let mut postfix = Postfix::new(app_cfg.postfix.clone(), None).expect("postfix component init failed");

                    let username = postfix_add_mailbox_user_m.value_of("username").expect("username address missing");
                    let domain = postfix_add_mailbox_user_m.value_of("domain").expect("domain address missing");
                    let password = postfix_add_mailbox_user_m.value_of("password").expect("password missing");
                    let quota_gb = postfix_add_mailbox_user_m.value_of("mailbox_quota_gb").unwrap_or("0").parse().expect("invalid quota");

                    let home = format!("{}/{}", &postfix.mail_directory(), &username);

                    let new_user = MailboxUser {
                        domain: String::from(domain),
                        password: String::from(password),
                        username: String::from(username),
                        home,
                        quota_gb,
                    };

                    let _ = postfix.db.connect().expect("failed to connect to db");
                    postfix.db.add_mailbox_user(new_user).expect("mailbox user creation failed:");

                    info!("successfully created new mailbox user with username address [{}]", &username);

                }, // END `postmgr postfix mailbox-user-add ...`

                // handle `postmgr postfix mailbox-user-remove ...`
                ("mailbox-user-remove", Some(postfix_remove_mailbox_user_m)) => {

                    // Create components
                    let mut postfix = Postfix::new(app_cfg.postfix.clone(), None).expect("postfix component init failed");

                    let username = postfix_remove_mailbox_user_m.value_of("username").expect("username missing");

                    let _ = postfix.db.connect().expect("failed to connect to db");
                    postfix.db.remove_mailbox_user_by_username(String::from(username)).expect("mailbox user removal failed:");

                    info!("successfully removed mailbox user with username address [{}]", &username);

                }, // END `postmgr postfix mailbox-user-remove ...`

                _ => panic!("Unrecognized postfix subcommand!")
            }

        },

        _ => panic!("Unrecognized subcommand!"),
    }
}
