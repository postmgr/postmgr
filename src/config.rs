use std::convert::From;
use std::fmt;
use std::fs::File;
use std::io::{Read, Error as IOError};
use std::net::Ipv4Addr;
use toml::de::Error as TomlError;
use toml;

////////////
// Errors //
////////////

#[derive(Debug)]
pub enum ConfigLoadError {
    EmptyFilePath,
    IO(IOError),
    TomlParse(TomlError)
}

impl fmt::Display for ConfigLoadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &ConfigLoadError::EmptyFilePath => write!(f, "No file path was specified"),
            &ConfigLoadError::IO(ref e) => write!(f, "IO error while building: {}", e),
            &ConfigLoadError::TomlParse(ref e) => write!(f, "TOML parsing error building: {}", e)
        }
    }
}

impl PartialEq for ConfigLoadError {
    fn eq(&self, other: &ConfigLoadError) -> bool {
        match self {
            &ConfigLoadError::EmptyFilePath => match other { &ConfigLoadError::EmptyFilePath => true, _ => false },
            // TODO: Deeper equality check that just basic types?
            &ConfigLoadError::IO(_) => match other { &ConfigLoadError::IO(_) => true, _ => false },
            &ConfigLoadError::TomlParse(_) => match other { &ConfigLoadError::TomlParse(_) => true, _ => false }
        }
    }
}

impl From<IOError> for ConfigLoadError {
    fn from(e: IOError) -> ConfigLoadError { ConfigLoadError::IO(e) }
}

impl From<TomlError> for ConfigLoadError {
    fn from(e: TomlError) -> ConfigLoadError { ConfigLoadError::TomlParse(e) }
}

////////////
// Types  //
////////////

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SQLiteDBCfg {
    pub in_memory: bool,
    pub path: String,
}

#[allow(dead_code)] // SqliteDBCfg is read from file but constructors only used from tests
impl SQLiteDBCfg {
    pub fn new(path: &str) -> SQLiteDBCfg {
        SQLiteDBCfg {
            path: String::from(path),
            in_memory: false
        }
    }

    pub fn in_memory() -> SQLiteDBCfg {
        SQLiteDBCfg {
            in_memory: true,
            path: String::from("")
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PostgresDBCfg {
    host: String,
    user: String,
    password: String,
    dbname: String,
    schema: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MilterCfg {
    pub non_smtpd_milters: Vec<String>,
    pub smtpd_milters: Vec<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SMTPDTLSCfg {
    pub enabled: bool,
    pub ca_file_path: String,
    pub cert_file_path: String,
    pub key_file_path: String,
    pub add_received_header: bool,
    pub sasl_tls_only: bool,
    pub session_cache_db: String,
    pub log_level: i32,
    pub session_cache_timeout_seconds: i32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SMTPTLSCfg {
    pub enabled: bool,
    pub ca_file_path: String,
    pub session_cache_db: String,
    pub log_level: i32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PostfixCfg {
    // Path to binaries
    pub bin_path: String, // postfix bin path
    pub sendmail_bin_path: String,
    pub mailq_bin_path: String,
    pub newaliases_bin_path: String,

    pub config_dir: String,
    pub config_output_dir: String,

    postmaster_user: String,

    pub internet_hostname: String,
    pub local_hostname: String,
    pub default_domain_name: String,
    pub inbound_domain_names: Vec<String>,
    pub allowed_relay_cidrs: Vec<String>,
    pub allowed_relay_destinations: Vec<String>,

    pub virtual_mailbox_domains: Vec<String>,

    pub delivery_relay_host: Option<String>,

    pub postmaster_notification_classes: Vec<String>,

    pub proxy_address: Option<String>,

    pub recipient_delimiter: String,

    pub submission_qmgr_group_name: String,

    pub mail_spool_directory: String,
    pub message_size_limit_bytes: i32,
    pub mailbox_size_limit_bytes: Option<i32>,

    // Constituent configs
    pub smtp_tls: Option<SMTPTLSCfg>,
    pub smtpd_tls: Option<SMTPDTLSCfg>,
    pub milters: Option<MilterCfg>,

    pub db: DBCfg,
    pub auth: AuthCfg
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct WebAdminCfg {
    pub host: Ipv4Addr,
    pub port: i32,

    // root user creds for first time start
    pub root_user_email: Option<String>,
    pub root_user_password: Option<String>,

    // Directory from which the frontend will be hosted
    pub frontend_dir: String,

    // DB used by the web admin (likely same as postfix)
    pub db: DBCfg
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DBCfg {
    pub backend: DBType,
    pub sqlite: Option<SQLiteDBCfg>,
    pub postgres: Option<PostgresDBCfg>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AuthCfg {
    pub backend: AuthBackendType,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AppCfg {
    pub file_path: Option<String>,
    pub postfix: PostfixCfg,
    pub dovecot: DovecotCfg,
    pub web_admin: WebAdminCfg,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum DBType {
    SQLite,
    PostgreSQL
}

impl From<String> for DBType {
    fn from(str: String) -> DBType {
        match str.as_ref() {
            "sqlite" => DBType::SQLite,
            "postgresql" => DBType::PostgreSQL,
            _ => panic!("Invalid database type {}", str)
        }
    }
}

/// For objects (mostly enums) that have a dovecot setting name
pub trait DovecotSettingName {
    fn dovecot_setting_name(self) -> &'static str;
}

impl DovecotSettingName for DBType {
    fn dovecot_setting_name(self) -> &'static str {
        match self {
            DBType::SQLite => "sqlite",
            DBType::PostgreSQL => "pgsql",
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum AuthBackendType {
    Dovecot
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DovecotCfg {
    pub mail_location: String,

    pub bin_path: String,
    pub doveadm_bin_path: String,
    pub config_output_dir: String,

    pub shared_uid_user: Option<String>,
    pub shared_gid_user: Option<String>,

    pub unix_socket_path: String,

    pub userdb_settings: Option<DovecotDBSettings>,
    pub passdb_settings: Option<DovecotDBSettings>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DovecotDBSettings {
    pub driver: DBType,
    pub connect: String,
    pub query: String,
    pub iterate_query: String,
}

///////////////
// Functions //
///////////////

// Builds a `AppCfg` from a TOML file at a given path.
//
// # Examples
// ```
// use std::path::Path;
//
// let cfg = build_postfix_cfg_from_toml(Path::new("./conf.toml"))
// assert_eq!(cfg.is_some(), true)
//```
fn build_cfg_from_toml(path: &str) -> Result<AppCfg, ConfigLoadError> {
    let mut f = File::open(path)?;
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    let mut cfg: AppCfg = toml::from_str(&contents)?;
    cfg.file_path = Some(String::from(path));

    return Ok(cfg);
}

pub fn gather_config(path: String) -> Result<AppCfg, ConfigLoadError> {
    let trimmed = path.trim();
    if trimmed.is_empty() { return Err(ConfigLoadError::EmptyFilePath); }

    return build_cfg_from_toml(trimmed);
}

#[cfg(test)]
mod test {
    use super::ConfigLoadError;
    use super::gather_config;

    #[test]
    fn errors_on_empty_files() {
        let result = gather_config(String::new());
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), ConfigLoadError::EmptyFilePath);
    }
}
