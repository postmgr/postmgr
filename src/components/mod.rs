pub mod postfix;
pub mod dovecot;
pub mod web_admin;
pub mod db;

use std::fmt;
use std::io::Error as IOError;
use std::process::{Command, Child as ChildProcess};
use askama::Error as AskamaError;
use rusqlite::Error as SQLiteError;
use std::sync::mpsc::SyncSender;
use std::any::Any;

////////////
// Traits //
////////////

pub trait Component {
    // Name fo the component
    fn get_name(&self) -> &str;

    // Start starts the component and *should not* return, often kicking off a thread or daemonized remote process with management
    fn start(&mut self) -> Result<(), Error>;

    // Does whatever necessary to stop the given component
    fn stop(&mut self) -> Result<(), Error>;

    // Restart the component, by default this is stop & start
    fn restart(&mut self) -> Result<(), Error> {
        self.stop()?;
        return self.start();
    }
}

pub trait HasReqRespCommandBus where Self: Component {
    type Msg;

    fn get_cmd_bus_tx(&self) -> Result<SyncSender<ReqResp<Self::Msg>>, Error>;
}

pub trait ChildProcessManager where Self: Component {
    fn spawn_child_process(&mut self) -> Result<&Option<ChildProcess>, Error>;
    fn get_pid(&self) -> Option<u32>;
    fn get_command(&self) -> &Option<Command>;
    fn get_process(&self) -> &Option<ChildProcess>;
}

// Commands used to trigger lifecycle for components
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ComponentCmd {
    Shutdown,
}

// For request/response communication between components
#[derive(Debug)]
pub struct ReqResp<M> {
    msg: M,
    pub response_chan: Option<SyncSender<Box<Any + Send>>>
}

pub trait Configurable<C>: Component {
    // Update & reload configuration for the component, possibly restarting in the process.
    fn update_config(&mut self, updated: Option<C>) -> Result<(), Error>;

    // Retrieve configuration for a component
    fn get_config(&self) -> &C;
}

pub trait FileConfigurable<C>: Configurable<C> {
    // Generate configuration files in a given directory
    fn generate_config_in_dir(&self, dir: Option<String>) -> Result<(), Error>;

    // Install configuration files in the appropriate directory
    fn install_config(&self) -> Result<(), Error>;

    // Configuration for the component will be stored at the path provided
    fn config_dir_path(&self) -> String;

}

// TODO: add a StandaloneComponent trait?

////////////
// Errors //
////////////

#[derive(Debug)]
pub enum Error {
    StartFailed,
    AlreadyRunningError,
    NotRunningError,
    #[allow(dead_code)] // NotSupported is usually used temporarily during development
    NotSupported,
    InvalidOrMissingConfig(&'static str),
    Disconnected,
    IO(IOError),
    TemplateError(AskamaError),
    InvalidEnvironment(&'static str),
    InvalidRuntimeEnvironment(String),
    DBError(String),
    SQLiteDBError(SQLiteError),
    EncryptionError(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Error::StartFailed => write!(f, "Failed to start component"),
            &Error::AlreadyRunningError => write!(f, "Component process is already running"),
            &Error::NotRunningError => write!(f, "Component process is not currently running"),
            &Error::NotSupported => write!(f, "Action is not supported on this component"),
            &Error::InvalidOrMissingConfig(ref str) => write!(f, "Invalid/missing component configuration: {}", str),
            &Error::Disconnected => write!(f, "component is disconnected"),
            &Error::IO(ref e) => write!(f, "IO error while building: {}", e),
            &Error::TemplateError(ref e) => write!(f, "Error while using template: {}", e),
            &Error::InvalidEnvironment(ref e) => write!(f, "Invalid environment (precondition failure): {}", e),
            &Error::InvalidRuntimeEnvironment(ref e) => write!(f, "Invalid environment @ runtime: {}", e),
            &Error::DBError(ref info) => write!(f, "Database error: {}", info),
            &Error::SQLiteDBError(ref e) => write!(f, "SQLite DB error: {}", e),
            &Error::EncryptionError(ref e) => write!(f, "Encryption error: {}", e),
        }
    }
}

impl From<IOError> for Error {
    fn from(e: IOError) -> Error { Error::IO(e) }
}

impl From<AskamaError> for Error {
    fn from(e: AskamaError) -> Error { Error::TemplateError(e) }
}

impl From<SQLiteError> for Error {
    fn from(e: SQLiteError) -> Error { Error::DBError(e.to_string()) }
}
