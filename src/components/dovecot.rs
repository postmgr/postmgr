use std::fs::{File, copy};
use std::io::Write;
use std::path::Path;
use std::process::{Command, Stdio, Child as ChildProcess};
use std::sync::mpsc::{sync_channel, SyncSender, Receiver};

use askama::Template;
use chrono::prelude::Local;
use make_absolute_path;
use ensure_directory;
use simple_signal::{Signal};
use simple_signal;

use components::*;
use config::{DovecotCfg, DovecotDBSettings, DovecotSettingName};


const DOVECOT_CONF_FILENAME: &'static str = "dovecot.conf";
const DOVECOT_USERDB_CONF_FILENAME: &'static str = "dovecot-sql-userdb.conf.ext";
const DOVECOT_PASSDB_CONF_FILENAME: &'static str = "dovecot-sql-passdb.conf.ext";

const DEFAULT_SHARED_UID_USER: &'static str = "vmail";
const DEFAULT_SHARED_GID_USER: &'static str = "vmail";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum DovecotCmd {
    Lifecycle(ComponentCmd),
}

pub struct Dovecot {
    cfg: DovecotCfg,

    cmd_bus_rx: Receiver<ReqResp<DovecotCmd>>,
    cmd_bus_tx: SyncSender<ReqResp<DovecotCmd>>,

    pid: Option<u32>,
    command: Option<Command>,
    process: Option<ChildProcess>,
}

impl Dovecot {
    pub fn new(cfg: DovecotCfg) -> Result<Dovecot, Error> {
        // Create command bus channels
        let (cmd_bus_tx, cmd_bus_rx) = sync_channel(0);

        Ok(Dovecot {
            cfg,
            cmd_bus_tx,
            cmd_bus_rx,
            pid: None,
            command: None,
            process: None,
        })
    }
}

impl Component for Dovecot {
    fn get_name(&self) -> &str { "Dovecot" }

    fn start(&mut self) -> Result<(), Error> {
        if let Some(_) = self.process { return Err(Error::AlreadyRunningError); }

        self.spawn_child_process()?;

        // Control loop, this never exits
        for req in self.cmd_bus_rx.iter() {
            match req.msg {
                DovecotCmd::Lifecycle(ComponentCmd::Shutdown) => {
                    debug!("received shutdown command to component");
                    break;
                },
            }
        }

        // Stop the child process since we've left the control loop
        self.stop()?;

        Ok(())
    }

    fn stop(&mut self) -> Result<(), Error> {
        if self.process.is_none() { return Err(Error::NotRunningError); }

        // assuming that if a process is present a PID is present
        let pid = self.pid.unwrap();
        debug!("killing dovecot process (PID {})...", &pid);

        if let Some(ref mut p) = self.process {
            // TODO: Figure out a better way to do this that doesn't use the binary directly
            // would be nice to have multiple postmgr instances running at one time without
            // manpages say sending SIGTERM would do but rust can't handle signals yet...
            // https://github.com/rust-lang/rfcs/issues/1368
            let stop_output = Command::new(&self.cfg.doveadm_bin_path)
                .arg("stop")
                .output()
                .expect("failed to run `doveadm stop`");

            if !stop_output.status.success() {
                debug!("`doveadm stop` failed, sending SIGTERM and killing the process from rust");
                Command::new("kill")
                    .arg("-TERM")
                    .arg(&pid.to_string())
                    .output()
                    .expect("failed to send kill signal to dovecot");

                p.kill()?;
            }

            // Wait for the process to exit completely
            p.wait()?;
        }

        return Ok(());
    }

}

impl HasReqRespCommandBus for Dovecot {
    type Msg = DovecotCmd;

    fn get_cmd_bus_tx(&self) -> Result<SyncSender<ReqResp<Self::Msg>>, Error> {
        Ok(self.cmd_bus_tx.clone())
    }
}

impl ChildProcessManager for Dovecot {
    fn spawn_child_process(&mut self) -> Result<&Option<ChildProcess>, Error> {
        debug!("spawning dovecot process...");

        // Build & save command to start dovecot in *non* daemonized mode so we can control it
        let args = &["-F"];
        let mut cmd = Command::new(&self.cfg.bin_path);
        cmd.args(args);
        cmd.stdin(Stdio::null());
        cmd.stdout(Stdio::inherit());
        debug!("command: [{:#?}]!", cmd);
        self.command = Some(cmd);

        // Actually run the command
        // NOTE: unfortunately we have to build it all over again since Command can't be cloned
        let child = Command::new(&self.cfg.bin_path)
            .args(args)
            .stdin(Stdio::null())
            //.stdout(Stdio::inherit())
            .spawn()?;
        self.pid = Some(child.id());
        self.process = Some(child);

        // Setup signal handlers with a channel
        let dovecot_tx = self.get_cmd_bus_tx()?;
        simple_signal::set_handler(&[Signal::Int, Signal::Term], move |_signals| {
            info!("Received SIGINT/SIGTERM, forwarding to all components");
            dovecot_tx.send(ReqResp{
                msg: DovecotCmd::Lifecycle(ComponentCmd::Shutdown),
                response_chan: None
            }).expect("failed to send shutdown command to dovecot component");
        });

        Ok(&self.process)
    }

    fn get_pid(&self) -> Option<u32> { self.pid }
    fn get_command(&self) -> &Option<Command> { &self.command }
    fn get_process(&self) -> &Option<ChildProcess> { &self.process }
}

impl Configurable<DovecotCfg> for Dovecot {
    fn update_config(&mut self, _cfg: Option<DovecotCfg>) -> Result<(), Error> {
        info!("updating dovecot configuration...");

        // Generate & install configuration files
        self.generate_config_in_dir(None)?;
        self.install_config()?;

        // Run `doveadm reload`
        Command::new(&self.cfg.doveadm_bin_path)
            .args(&["reload"])
            .output()?;

        Ok(())
    }

    fn get_config(&self) -> &DovecotCfg { &self.cfg }
}

#[derive(Template)]
#[template(path = "config/dovecot/dovecot.conf.jinja")]
struct DovecotConfTemplate<'a> {
    filename: &'a str,
    generation_time: String,

    mail_location: &'a String,
    unix_socket_path: &'a String,

    shared_uid_user: &'a String,
    shared_gid_user: &'a String,

    userdb: &'a Option<DovecotDBSettings>,
    userdb_conf_abs_path: String,

    passdb: &'a Option<DovecotDBSettings>,
    passdb_conf_abs_path: String,
}

#[derive(Template)]
#[template(path = "config/dovecot/dovecot-sql-userdb.conf.ext.jinja")]
struct DovecotUserDBConfTemplate<'a> {
    filename: &'a str,
    generation_time: String,
    settings: &'a DovecotDBSettings,
}

#[derive(Template)]
#[template(path = "config/dovecot/dovecot-sql-passdb.conf.ext.jinja")]
struct DovecotPassDBConfTemplate<'a> {
    filename: &'a str,
    generation_time: String,
    settings: &'a DovecotDBSettings,
}

impl FileConfigurable<DovecotCfg> for Dovecot {
    fn generate_config_in_dir(&self, dir: Option<String>) -> Result<(), Error> {
        info!("updating on-disk configuration for dovecot");

        // Ensure configuration directory exists
        let dir = dir.unwrap_or(self.config_dir_path());
        ensure_directory(&dir)?;

        // Generate file paths for the dovecot.conf file
        let output_dir = Path::new(&self.cfg.config_output_dir);
        let main_conf_path = output_dir.join(DOVECOT_CONF_FILENAME);
        let main_conf_abs_path = make_absolute_path(main_conf_path.to_path_buf())?;

        // Generate *possible* file path for userdb/passdb configuration
        let userdb_conf_path = output_dir.join(DOVECOT_USERDB_CONF_FILENAME);
        let userdb_conf_abs_path = make_absolute_path(userdb_conf_path.to_path_buf())?;
        let passdb_conf_path = output_dir.join(DOVECOT_PASSDB_CONF_FILENAME);
        let passdb_conf_abs_path = make_absolute_path(passdb_conf_path.to_path_buf())?;

        // Build dovecot conf template
        let main_conf_tmpl = DovecotConfTemplate {
            filename: DOVECOT_CONF_FILENAME,
            generation_time: Local::now().to_string(),

            mail_location: &self.cfg.mail_location,
            unix_socket_path: &self.cfg.unix_socket_path,

            shared_uid_user: &self.cfg.shared_uid_user.clone().unwrap_or(DEFAULT_SHARED_UID_USER.to_string()),
            shared_gid_user: &self.cfg.shared_gid_user.clone().unwrap_or(DEFAULT_SHARED_GID_USER.to_string()),
            
            userdb: &self.cfg.userdb_settings,
            userdb_conf_abs_path: userdb_conf_abs_path,

            passdb: &self.cfg.passdb_settings,
            passdb_conf_abs_path: passdb_conf_abs_path,
        };


        // Render the main config
        debug!("{} will be written to [{}]", &main_conf_tmpl.filename, &main_conf_abs_path);
        let mut main_conf_file = File::create(main_conf_path)?;
        main_conf_file.write_all(main_conf_tmpl.render()?.as_bytes())?;

        // Write the userdb sql conf if settings were specified
        if let Some(settings) = &self.cfg.userdb_settings {
            // Build userdb SQL configuration template
            let tmpl = DovecotUserDBConfTemplate {
                filename: DOVECOT_USERDB_CONF_FILENAME,
                generation_time: Local::now().to_string(),
                settings,
            };

            let conf_path = output_dir.join(DOVECOT_USERDB_CONF_FILENAME);
            let conf_abs_path = make_absolute_path(conf_path.to_path_buf())?;

            // Render the main config
            debug!("{} will be written to [{}]", &tmpl.filename, &conf_abs_path);
            let mut conf_file = File::create(conf_path)?;
            conf_file.write_all(tmpl.render()?.as_bytes())?;
        }

        // Write the passdb sql conf if settings were specified
        if let Some(settings) = &self.cfg.passdb_settings {
            // Build passdb SQL configuration template
            let tmpl = DovecotPassDBConfTemplate {
                filename: DOVECOT_PASSDB_CONF_FILENAME,
                generation_time: Local::now().to_string(),
                settings,
            };

            let conf_path = output_dir.join(DOVECOT_PASSDB_CONF_FILENAME);
            let conf_abs_path = make_absolute_path(conf_path.to_path_buf())?;

            // Render the main config
            debug!("{} will be written to [{}]", &tmpl.filename, &conf_abs_path);
            let mut conf_file = File::create(conf_path)?;
            conf_file.write_all(tmpl.render()?.as_bytes())?;
        }

        debug!("successfully wrote dovecot configuration files to disk");
        Ok(())
    }

    fn install_config(&self) -> Result<(), Error> {
        // Generate the directories the files are going to go into
        let config_dir_path = Path::new(&self.cfg.config_output_dir);
        let config_output_dir_path = Path::new(&self.cfg.config_output_dir);
        debug!(
            "installing config file(s) from directory [{:?}] to [{:?}]",
            config_dir_path.to_str(),
            config_output_dir_path.to_str(),
        );

        // Ensure configuration directory exists
        ensure_directory(&self.cfg.config_output_dir)?;

        // Copy dovecot.conf
        let dovecot_conf_from = config_dir_path.clone().join(DOVECOT_CONF_FILENAME);
        let dovecot_conf_to = config_output_dir_path.clone().join(DOVECOT_CONF_FILENAME);
        if dovecot_conf_from != dovecot_conf_to {
            debug!("copying dovecot.conf from [{:?}] to [{:?}]", dovecot_conf_from, dovecot_conf_to);
            copy(&dovecot_conf_from, &dovecot_conf_to)?;
        }

        // Copy dovecot userdb conf
        let userdb_conf_from = config_dir_path.clone().join(DOVECOT_USERDB_CONF_FILENAME);
        let userdb_conf_to = config_output_dir_path.clone().join(DOVECOT_USERDB_CONF_FILENAME);
        if userdb_conf_from != userdb_conf_to {
            debug!(
                "copying {} from [{:?}] to [{:?}]",
                DOVECOT_USERDB_CONF_FILENAME,
                userdb_conf_from,
                userdb_conf_to,
            );
            copy(&userdb_conf_from, &userdb_conf_to)?;
        }

        // Copy dovecot passdb conf
        let passdb_conf_from = config_dir_path.clone().join(DOVECOT_PASSDB_CONF_FILENAME);
        let passdb_conf_to = config_output_dir_path.clone().join(DOVECOT_PASSDB_CONF_FILENAME);
        if passdb_conf_from != passdb_conf_to {
            debug!(
                "copying {} from [{:?}] to [{:?}]",
                DOVECOT_PASSDB_CONF_FILENAME,
                passdb_conf_from,
                passdb_conf_to,
            );
            copy(&passdb_conf_from, &passdb_conf_to)?;
        }

        Ok(())
    }

    fn config_dir_path(&self) -> String { self.cfg.config_output_dir.clone() }
}
