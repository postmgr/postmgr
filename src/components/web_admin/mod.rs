mod api;
mod root;

use actix_web::server;
use components::postfix::PostfixCmd;
use components::dovecot::DovecotCmd;
use components::web_admin::api::v1::{app as api_v1_app};
use components::web_admin::root::{app as root_app};
use components::{Component, ReqResp, ComponentCmd, Error};
use config::WebAdminCfg;
use std::sync::mpsc::SyncSender;

#[allow(dead_code)]
pub struct WebAdmin {
    cfg: WebAdminCfg,

    /// For communicating with a postfix component
    postfix_tx_chan: SyncSender<ReqResp<PostfixCmd>>,

    /// For communicating with a dovecot component
    dovecot_tx_chan: SyncSender<ReqResp<DovecotCmd>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum WebAdminCmd {
    Lifecycle(ComponentCmd)
}

#[derive(Default)]
pub struct AppState {
    postfix_tx: Option<SyncSender<ReqResp<PostfixCmd>>>,
    dovecot_tx: Option<SyncSender<ReqResp<DovecotCmd>>>,
}

impl WebAdmin {
    pub fn new(
        cfg: WebAdminCfg,
        postfix_tx_chan: SyncSender<ReqResp<PostfixCmd>>,
        dovecot_tx_chan: SyncSender<ReqResp<DovecotCmd>>,
    ) -> Result<WebAdmin, Error> {
        Ok(WebAdmin { cfg, postfix_tx_chan, dovecot_tx_chan })
    }
}

impl Component for WebAdmin {
    fn get_name(&self) -> &str { "WebAdmin" }

    fn start(&mut self) -> Result<(), Error> {
        let address = format!("{}:{}", self.cfg.host, self.cfg.port);
        let postfix_chan = self.postfix_tx_chan.clone();
        let dovecot_chan = self.dovecot_tx_chan.clone();

        server::new(move || {
            vec![
                api_v1_app(postfix_chan.clone(), dovecot_chan.clone()),
                root_app(),
            ]
        })
            .bind(address)
            .unwrap()
            .run();

        Ok(())
    }

    fn stop(&mut self) -> Result<(), Error> {
        // FIX: Since the server never returns it has to get stopped by CTRL+C
        // it would be nice if the server itself could be owned by the object and stopped properly,
        // but it's not clear how to do that with actix_web, since even if you start it in a thread, thread killing isn't a thing in rust
        // if start() blocks, then stop() can never get called,
        // if start() spawns a thread then control-loops can't call system_exit on the server (since it's been given to the thread).
        Ok(())
    }
}
