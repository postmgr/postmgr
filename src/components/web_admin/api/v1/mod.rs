use actix_web::error::*;
use actix_web::http::Method;
use actix_web::{App, HttpRequest, HttpResponse, Json, Result, Path, State};
use components::ReqResp;
use components::postfix::PostfixCmd;
use components::dovecot::DovecotCmd;
use components::web_admin::AppState;
use models::user::MailboxUser;
use models::ModelWithUUID;
use std::sync::mpsc::{sync_channel, SyncSender};
use std::time::Duration;
use serde_json::json;

const DEFAULT_OPERATION_TIMEOUT: Duration = Duration::from_secs(5);

pub fn app(
    postfix_tx: SyncSender<ReqResp<PostfixCmd>>,
    dovecot_tx: SyncSender<ReqResp<DovecotCmd>>,
) -> App<AppState> {
    let state = AppState {
        postfix_tx: Some(postfix_tx),
        dovecot_tx: Some(dovecot_tx),
    };

    App::with_state(state)
        .prefix("/api/v1")
        .resource("", |r| r.f(api_index))
        .resource("components/{name}/status", |r| r.method(Method::GET).with(component_status))
        .resource("users", |r| r.method(Method::POST).with(add_user))

}

#[derive(Serialize)]
struct APIStatus {
    version: &'static str,
}

// TODO: Move out of here and into the components module
#[derive(Serialize)]
struct ComponentStatus {
    name: String,
    state: &'static str,
}

fn api_index(_req: &HttpRequest<AppState>) -> Result<Json<APIStatus>> {
    Ok(Json(APIStatus{
        version: &env!("CARGO_PKG_VERSION")
    }))
}

// Get the status of a given component
fn component_status(info: Path<String>) -> Result<Json<ComponentStatus>> {
    // TODO: Actually get the component to return it's status, by calling some get_status() command on the relevant component?
    Ok(Json(ComponentStatus{
        name: info.to_string(),
        state: "running"
    }))
}

/// Add a user
fn add_user(state: State<AppState>, new_user: Json<MailboxUser>) -> Result<HttpResponse> {
    // Create request to send to postfix component
    let (tx, rx) = sync_channel(0);
    let postfix_req = ReqResp::<PostfixCmd> {
        msg: PostfixCmd::CreateUser(new_user.into_inner()),
        response_chan: Some(tx),
    };

    // Ensure postfix tx is present
    if state.postfix_tx.is_none() { return Err(ErrorInternalServerError("No postfix component present")); }

    // Send the request to the postfix component
    state.postfix_tx
        .as_ref()
        .unwrap()
        .send(postfix_req)
        .map_err(|_| ErrorInternalServerError("Failed contacting postfix component"))?;

    // Wait for the response from postfix, which should contain the mailbox user
    rx
        .recv_timeout(DEFAULT_OPERATION_TIMEOUT)
        .map(|b| match b.downcast::<ModelWithUUID<MailboxUser>>() {

            Ok(u) => HttpResponse::Created().json(json!({
                "status": "success",
                "data": u,
            })),

            Err(_) => HttpResponse::InternalServerError().json(json!({
                "status": "error",
                "message": "Failed to create user",
            })).into(),

        })
        .map_err(|_| ErrorInternalServerError("No response from Postfix component"))
}
