use actix_web::{App, HttpRequest, fs::NamedFile, fs::StaticFiles, Result};
use components::web_admin::AppState;

pub fn app() -> App<AppState> {
    // TODO: provide 'frontend/' using frontend_dir app config variable
    App::with_state(AppState::default())
        .prefix("/")
        .resource("/", |r| r.f(serve_index_html))
        .handler("/js", StaticFiles::new("frontend/js").unwrap())
        .handler("/images", StaticFiles::new("frontend/images").unwrap())
}

fn serve_index_html(_req: &HttpRequest<AppState>) -> Result<NamedFile> {
    Ok(NamedFile::open("frontend/index.html")?)
}
