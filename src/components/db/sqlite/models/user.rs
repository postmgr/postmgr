use components::Error;
use components::db::sqlite::SQLiteDB;
use models::user::MailboxUser;
use models::{DBEntity, HasSQLTable, ModelWithUUID, PaginationOptions, PaginatedList};
use rusqlite::Row;
use rusqlite::types::ToSql;
use super::super::*;

const COLUMNS: &'static [&'static str] = &["uuid", "username", "domain", "home", "password", "quota_gb"];

impl HasSQLTable<SQLiteDB> for ModelWithUUID<MailboxUser> {
    fn sql_tbl_name() -> &'static str { "mailbox_users" }
    fn sql_tbl_columns() -> &'static [&'static str] { COLUMNS }
}

impl<'a, 'stmt> DBEntity<SQLiteDB, Row<'a, 'stmt>> for ModelWithUUID<MailboxUser> {
    fn from_row(row: &Row) -> Result<Self, Error> {
        let uuid = row.get_checked("uuid")?;
        let username = row.get_checked("username")?;
        let domain = row.get_checked("domain")?;
        let home = row.get_checked("home")?;
        let password = row.get_checked("password")?;
        let quota_gb = row.get_checked("quota_gb")?;
        let model = MailboxUser { username, domain, password, home, quota_gb };
        Ok(ModelWithUUID { uuid, model })
    }

    fn insert(&self, db: &SQLiteDB) -> Result<&Self, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(db)?;
        let mut stmt = gen_insert_stmt(c, Self::sql_tbl_name(), Self::sql_tbl_columns())?;

        let inserted_row_count = stmt.execute(&[
            &self.uuid as &ToSql,
            &self.model.username,
            &self.model.domain,
            &self.model.home,
            &self.model.password,
            &self.model.quota_gb
        ])?;
        if inserted_row_count == 1 { return Ok(self); }

        Err(Error::DBError(String::from("Statement executed but unaffected row count returned: insertion (likely) failed")))
    }

    fn list(db: &SQLiteDB, pagination: &PaginationOptions) -> Result<PaginatedList<ModelWithUUID<MailboxUser>>, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(db)?;
        let (mut stmt, mut total_stmt) = gen_select_with_count_stmt(c, Self::sql_tbl_name())?;

        // Generate limit/offset to be used
        let limit = pagination.get_limit_or_default();
        let offset = pagination.get_offset_or_default();

        // Execute query
        let items_iter = stmt.query_map(&[&limit, &offset], |r| Self::from_row(r).unwrap())?;
        let items: Vec<Self> = items_iter.map(Result::unwrap).collect();

        let total: u32 = total_stmt.query_row(&[], |r| r.get(0))?;

        Ok(PaginatedList{items, total})
    }

    fn delete_by_uuid(db: &SQLiteDB, uuid: &String) -> Result<(), Error> {
        let c: &SQLiteConnection = get_sqlite_connection(db)?;
        let mut stmt = gen_delete_by_field_stmt(c, Self::sql_tbl_name(), "uuid")?;

        // Perform query and return affected row count
        let deleted_row_count = stmt.execute(&[&uuid.as_str()])?;
        if deleted_row_count == 1 { return Ok(()); }

        Err(Error::DBError(format!("Statement executed but unexpected row count returned: MailboxUser (uuid: {}) not properly deleted", &uuid)))
    }

    fn delete(&self, db: &SQLiteDB) -> Result<(), Error> {
        Self::delete_by_uuid(db, &self.uuid)
    }

    fn find_by_uuids(db: &SQLiteDB, uuids: &[String]) -> Result<Vec<Self>, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(db)?;
        let mut stmt = gen_find_by_uuids_stmt(c, Self::sql_tbl_name(), uuids.len())?;

        // Generate query values to pass
        let uuids_as_str: Vec<&str> = uuids.iter().map(|s| s.as_str()).collect();
        let refs: Vec<&ToSql> = uuids_as_str.iter().map(|s| s as &ToSql).collect();

        // Perform query and return rows
        let results_iter = stmt.query_map(&refs, |r| Self::from_row(r).unwrap())?;
        let results = results_iter.map(Result::unwrap).collect();
        Ok(results)
    }

    fn find_by_text_field(db: &SQLiteDB, field: &str, value: String) -> Result<Vec<Self>, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(db)?;
        let mut stmt = gen_find_by_text_field_stmt(c, Self::sql_tbl_name(), field)?;

        let results_iter = stmt.query_map(&[&value], |r| Self::from_row(r).unwrap())?;
        let results = results_iter.map(Result::unwrap).collect();
        Ok(results)
    }
}

#[cfg(test)]
mod tests {
    use config::SQLiteDBCfg;
    use components::db::sqlite::SQLiteDB;
    use components::db::Connectable;
    use models::user::MailboxUser;
    use models::{DBEntity, ModelWithUUID, PaginationOptions};

    const TEST_USER_HOME: &'static str = "/var/mail/test";
    const TEST_USER_EMAIL: &'static str = "test@example.com";
    const TEST_USER_DOMAIN: &'static str = "localhost";
    const TEST_USER_PASSWORD: &'static str = "test";

    const ADMIN_USER_HOME: &'static str = "/var/mail/admin";
    const ADMIN_USER_EMAIL: &'static str = "admin@example.com";
    const ADMIN_USER_DOMAIN: &'static str = "localhost";
    const ADMIN_USER_PASSWORD: &'static str = "admin";

    fn make_test_user() -> MailboxUser {
        MailboxUser::new(
            String::from(TEST_USER_HOME),
            String::from(TEST_USER_DOMAIN),
            String::from(TEST_USER_EMAIL),
            String::from(TEST_USER_PASSWORD)
        )
    }

    fn make_admin_user() -> MailboxUser {
        MailboxUser::new(
            String::from(ADMIN_USER_DOMAIN),
            String::from(ADMIN_USER_HOME),
            String::from(ADMIN_USER_EMAIL),
            String::from(ADMIN_USER_PASSWORD),
        )
    }

    #[test]
    fn models_user_insert() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        let model = make_test_user();
        let entity = ModelWithUUID::from_model(model);

        let create_result = entity.insert(&db);
        assert!(create_result.is_ok(), "successfully created entity");
    }

    #[test]
    fn models_user_insert_and_list() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        let model = make_test_user();
        let entity = ModelWithUUID::from_model(model);

        let create_result = entity.insert(&db);
        assert!(create_result.is_ok(), "successfully created entity");

        let pagination = PaginationOptions::new();
        let list_result = ModelWithUUID::<MailboxUser>::list(&db, &pagination).expect("listing failed");

        assert_eq!(list_result.items.len(), 1, "returned paginated list has the right number of items");
        assert_eq!(list_result.total, 1, "returned paginated list has the right total");
        assert_eq!(list_result.items[0].uuid, entity.uuid, "returned entity ID matches saved entity");
    }

    #[test]
    fn models_user_delete_by_uuid() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        let model = make_test_user();
        let entity = ModelWithUUID::from_model(model);

        // create the user
        let _ = entity.insert(&db).expect("entity create failed");

        // delete the user
        let _ = ModelWithUUID::<MailboxUser>::delete_by_uuid(&db, &entity.uuid).expect("entity delete failed");

        // Getr a listing
        let pagination = PaginationOptions::new();
        let list_result = ModelWithUUID::<MailboxUser>::list(&db, &pagination).expect("listing failed");

        assert_eq!(list_result.items.len(), 0, "returned paginated list has the right number of items");
        assert_eq!(list_result.total, 0, "returned paginated list has the right total");
    }

    #[test]
    fn models_user_delete() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        let model = make_test_user();
        let entity = ModelWithUUID::from_model(model);

        // create the user
        let _ = entity.insert(&db).expect("entity create failed");

        // delete the user
        let _ = entity.delete(&db).expect("entity delete failed");

        // Getr a listing
        let pagination = PaginationOptions::new();
        let list_result = ModelWithUUID::<MailboxUser>::list(&db, &pagination).expect("listing failed");

        assert_eq!(list_result.items.len(), 0, "returned paginated list has the right number of items");
        assert_eq!(list_result.total, 0, "returned paginated list has the right total");
    }


    #[test]
    fn models_user_find_by_uuids() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        let test_model = make_test_user();

        let test_entity = ModelWithUUID::from_model(test_model);

        let admin_model = make_admin_user();
        let admin_entity = ModelWithUUID::from_model(admin_model);

        // create the user
        let _ = test_entity.insert(&db).expect("test entity create failed");
        let _ = admin_entity.insert(&db).expect("admin entity create failed");

        // find a single user
        let uuids = [admin_entity.uuid.clone()];
        let find_result = ModelWithUUID::<MailboxUser>::find_by_uuids(&db, &uuids).expect("listing failed");
        assert_eq!(find_result.len(), 1, "found one entity (admin) by uuid");
        assert_eq!(find_result[0].uuid, admin_entity.uuid, "returned entity is the right one (admin)");

        // find multiple users
        let uuids = [admin_entity.uuid.clone(), test_entity.uuid.clone()];
        let find_result = ModelWithUUID::<MailboxUser>::find_by_uuids(&db, &uuids).expect("listing failed");
        assert_eq!(find_result.len(), 2, "found both entities");
    }

    #[test]
    fn models_user_find_by_text_field() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        // create the user
        let model = make_test_user();
        let entity = ModelWithUUID::from_model(model);
        let _ = entity.insert(&db).expect("test entity create failed");

        // find the user by email field
        let find_result = ModelWithUUID::<MailboxUser>::find_by_text_field(
            &db,
            "username",
            String::from(TEST_USER_EMAIL),
        ).expect("listing failed");
        assert_eq!(find_result.len(), 1, "found one entity test user by email field");
        assert_eq!(find_result[0].uuid, entity.uuid, "returned entity is the right one");
    }
}
