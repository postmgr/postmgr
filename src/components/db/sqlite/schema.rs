use components::db::Migration::HardCodedSQL;
use components::db::Migration;
use super::HardCodedSQLMigration;

////////////////
// Migrations //
////////////////

pub const MAILBOX_MIGRATIONS: &'static [Migration] = &[
    HardCodedSQL(HardCodedSQLMigration {
        version: 1,
        sql: "
CREATE TABLE mailbox_users (
  uuid TEXT PRIMARY KEY,
  username TEXT NOT NULL,
  domain TEXT NOT NULL,
  home TEXT NOT NULL,
  password TEXT NOT NULL,
  quota_gb INTEGER DEFAULT 0,

  CONSTRAINT quota_gt_zero CHECK (quota_gb >= 0)
);

CREATE UNIQUE INDEX mailbox_users_username_domain_idx ON users(username, domain);
"
    }),
];

pub const ADMIN_MIGRATIONS: &'static [Migration] = &[];

#[cfg(test)]
mod tests {
    use super::*;
    use components::db::{SQLMigration, Connectable, SQLiteDB};
    use config::SQLiteDBCfg;

    #[test]
    fn mailbox_migrations_are_ordered() {
        for (idx, m) in MAILBOX_MIGRATIONS.iter().enumerate() {
            assert_eq!(idx as u32 + 1, m.version() , "migration has correct index");
        }
    }

    #[test]
    fn admin_migrations_are_ordered() {
        for (idx, m) in ADMIN_MIGRATIONS.iter().enumerate() {
            assert_eq!(idx as u32 + 1, m.version() , "migration has correct index");
        }
    }

    #[test]
    fn connection_detects_disconnected_db() {
        let db = SQLiteDB::new(SQLiteDBCfg::new(""));

        let result = db.connection();

        assert_eq!(result.is_err(), true, "connection returns an Err(...) when no connection is present");
    }

    #[test]
    fn connection_detects_connected_db() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());

        let _ = db.connect();
        let result = db.connection();

        assert_eq!(result.is_ok(), true, "connection returns a OK(connection)");
    }

}
