pub mod schema;
pub mod models;

use std::ffi::OsStr;
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::Path;

use askama::Template;
use chrono::prelude::Local;
use rusqlite::Connection as SqliteConnection;
use rusqlite::Statement;

use self::schema::{ADMIN_MIGRATIONS, MAILBOX_MIGRATIONS};
use components::Error;
use components::db::*;
use config::SQLiteDBCfg;
use models::user::MailboxUser;
use models::{ModelWithUUID, PaginatedList, PaginationOptions, DBEntity};
use make_absolute_path_from_str;

pub struct SQLiteDB {
    cfg: SQLiteDBCfg,
    conn: Option<DBConnection>
}

impl SQLiteDB {
    pub fn new(cfg: SQLiteDBCfg) -> SQLiteDB {
        SQLiteDB { cfg, conn: None }
    }

    pub fn make_absolute_db_path(&self) -> Result<String, Error> {
        make_absolute_path_from_str(&self.cfg.path)
    }
}

impl Connectable for SQLiteDB {
    fn connect(&mut self) -> Result<(), Error> {

        if self.cfg.in_memory {
            // Create in-memory DB
            self.conn = Some(DBConnection::SQLite(SqliteConnection::open_in_memory()?));

        } else {
            // Use supplied DB path if provided
            let sqlite_db_path = Path::new(&self.cfg.path);
            if !sqlite_db_path.exists() {
                warn!("db file @ [{:?}] was missing and is being auto-generated", sqlite_db_path.to_str());
                create_dir_all(&sqlite_db_path.parent().expect("Failed to get directory of db path"))?;
                File::create(sqlite_db_path)?;
            }

            if self.conn.is_some() { return Ok(()); }
            self.conn = Some(DBConnection::SQLite(SqliteConnection::open(&self.cfg.path)?));
        }

        // Initialize databases
        self.init_mailbox_db().expect("failed to initialize mailbox database");
        self.init_admin_db().expect("failed to initialize admin database");

        Ok(())
    }

    fn connection(&self) -> Result<&DBConnection, Error> {
        self.conn.as_ref().ok_or(Error::Disconnected)
    }
}

fn get_sqlite_connection<'a>(db: &'a Connectable) -> Result<&'a SQLiteConnection, Error> {
        match db.connection()? {
            &DBConnection::SQLite(ref conn) => Ok(conn),
            _ => Err(Error::NotSupported)
        }
}

impl Versioned<u32> for SQLiteDB {
    fn retrieve_version(&self) -> Result<u32, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(self)?;

        c.query_row("PRAGMA user_version;", &[], |row| {
            let version: u32 = row.get(0);
            version
        }).map_err(Error::from)
    }

    fn persist_version(&self, version: u32) -> Result<u32, Error> {
        let c: &SQLiteConnection = get_sqlite_connection(self)?;
        let query = format!("PRAGMA user_version = {};", version);
        c.execute(query.as_str(), &[]).map_err(Error::from)?;
        Ok(version)
    }
}

impl Migratable<u32> for SQLiteDB {
    fn run_migration(&self, m: &Migration) -> Result<(), Error> {
        let c: &SQLiteConnection = get_sqlite_connection(self)?;
        c.execute(m.sql().as_str(), &[])?;
        Ok(())
    }

    fn migrate_to_latest(&self, migrations: &[Migration]) -> Result<u32, Error> {
        let mut current_version = self.retrieve_version()?;

        // Return early if current version from DB is the latest (starts from 1)
        if current_version as usize >= migrations.len() { return Ok(current_version) }
        debug!("current DB version: v{}", &current_version);

        // Run each migration, updating the current version as we go
        for m in &migrations[current_version as usize..] {
            debug!("migrating DB v{} -> v{}", &current_version, &m.version());
            self.run_migration(m)?;
            current_version = m.version();
            self.persist_version(current_version)?;
        }

        Ok(current_version)
    }
}

impl MailboxDB for SQLiteDB {
    fn init_mailbox_db(&self) -> Result<(), Error> {
        if self.conn.is_none() { return Err(Error::Disconnected); }
        self.migrate_to_latest(MAILBOX_MIGRATIONS)?;
        Ok(())
    }

    fn add_mailbox_user(&self, mut u: MailboxUser) -> Result<ModelWithUUID<MailboxUser>, Error> {
        let db_model = ModelWithUUID::from_model(u);
        db_model.insert(self)?;
        Ok(db_model)
    }

    fn list_mailbox_users(&self, pagination: &PaginationOptions) -> Result<PaginatedList<ModelWithUUID<MailboxUser>>, Error> {
        ModelWithUUID::<MailboxUser>::list(self, pagination)
    }

    fn get_mailbox_users_by_uuids(&self, uuids: &[String]) -> Result<Vec<ModelWithUUID<MailboxUser>>, Error> {
        ModelWithUUID::<MailboxUser>::find_by_uuids(self, uuids)
    }

    fn remove_mailbox_user_by_username(&self, username: String) -> Result<(), Error> {
        let users = ModelWithUUID::<MailboxUser>::find_by_text_field(self, "username", username)?;
        if users.len() == 0 { return Err(Error::DBError(String::from("failed to find user with given username"))); }
        if users.len() > 1 { return Err(Error::DBError(String::from("found multiple users with given username"))); }
        users[0].delete(self)
    }
}

impl AdminDB for SQLiteDB {
    fn init_admin_db(&self) -> Result<(), Error> {
        if self.conn.is_none() { return Err(Error::Disconnected); }
        self.migrate_to_latest(ADMIN_MIGRATIONS)?;
        Ok(())
    }
}

// Generic method for generating the SQL statement used for performing a find by UUID
// This is here because find_by_uuid itself couldn't be completely generalized, due to associated lifetime funkiness on rusqlite::Row<'_, '_>
pub fn gen_find_by_uuids_stmt<'a>(c: &'a SqliteConnection, table_name: &'static str, uuid_count: usize) -> Result<Statement<'a>, Error> {
    let mut uuids_listing: Vec<String> = Vec::new();
    for idx in 0..uuid_count {
        uuids_listing.push(format!("?{}", idx + 1))
    }

    let stmt = format!("SELECT * FROM {} WHERE uuid IN ({})", table_name, uuids_listing.join(","));
    c.prepare(stmt.as_str()).map_err(Error::SQLiteDBError)
}

// Generic method for generating the SQL required to find by a given text field
pub fn gen_find_by_text_field_stmt<'a>(c: &'a SqliteConnection, table_name: &'static str, field: &str) -> Result<Statement<'a>, Error> {
    let query_str = format!("SELECT * FROM {} WHERE {} = ?1", table_name, field);
    c.prepare(query_str.as_str()).map_err(Error::SQLiteDBError)
}

// Generic method for generating the SQL required to find by a given text field
pub fn gen_delete_by_field_stmt<'a>(c: &'a SqliteConnection, table_name: &'static str, field: &str) -> Result<Statement<'a>, Error> {
    let query_str = format!("DELETE FROM {} WHERE {} = ?1", table_name, field);
    c.prepare(query_str.as_str()).map_err(Error::SQLiteDBError)
}

// Generic method for generating the SQL required to perform a basic select w/ count included
pub fn gen_select_with_count_stmt<'a>(c: &'a SqliteConnection, table_name: &'static str) -> Result<(Statement<'a>, Statement<'a>), Error> {
    let query_str = format!("SELECT * FROM {} LIMIT ?1 OFFSET ?2", table_name);
    let count_query_str = format!("SELECT COUNT(*) FROM {}", table_name);
    let stmt = c.prepare(query_str.as_str()).map_err(Error::SQLiteDBError)?;
    let count_stmt = c.prepare(count_query_str.as_str()).map_err(Error::SQLiteDBError)?;
    Ok((stmt, count_stmt))
}

// Generic method for generating the SQL required to perform a basic select w/ count included
pub fn gen_insert_stmt<'a>(c: &'a SqliteConnection, table_name: &'static str, fields: &'static [&'static str]) -> Result<Statement<'a>, Error> {
    let fields_listing = fields.join(",");
    let mut values_listing: Vec<String> = Vec::new();
    for (idx, _) in fields.iter().enumerate() {
        values_listing.push(format!("?{}", idx + 1))
    }

    let query_str = format!("INSERT INTO {} ({}) VALUES ({})", table_name, fields_listing, values_listing.join(","));
    c.prepare(query_str.as_str()).map_err(Error::SQLiteDBError)
}

#[derive(Template)]
#[template(path = "config/postfix/sqlite_aliases.cf.jinja")]
struct SQLiteVirtualAliasLookupConfigTemplate<'a> {
    filename: &'a str,
    generation_time: String,
    abs_db_path: String
}

impl SupportsVAliasLookup for SQLiteDB {
    fn write_valias_lookup_config_file(&self, output_path: &Path) -> Result<(), Error> {
        let filename = output_path.file_name()
            .unwrap_or(OsStr::new("<filename unspecified>"))
            .to_str()
            .unwrap_or("<filename unspecified>");

        let abs_db_path = self.make_absolute_db_path()?;
        info!("abs_db_path {}", &abs_db_path);

        // Populate template with data
        let template = SQLiteVirtualAliasLookupConfigTemplate {
            filename,
            generation_time: Local::now().to_string(),
            abs_db_path
        };

        // Ensure the output directory we're supposed to be writing to exists
        let output_dir = output_path.parent().ok_or(Error::InvalidOrMissingConfig("failed to decipher valias output directory"))?;
        if !output_dir.exists() { create_dir_all(&output_dir)?; }

        // Generate paths for configuration files
        let path_str = output_path.to_str().unwrap_or("<filename missing>");
        debug!("writing valias lookup config file @ [{:?}]", path_str);

        // Render the file contents, write them to disk
        let mut query_config_file = File::create(output_path)?;
        query_config_file.write_all(template.render()?.as_bytes())?;

        Ok(())
    }
}

#[derive(Template)]
#[template(path = "config/postfix/sqlite_mailbox.cf.jinja")]
struct SQLiteVirtualMailboxLookupConfigTemplate<'a> {
    filename: &'a str,
    generation_time: String,
    abs_db_path: String,
    mailbox_base_dir: String
}

impl SupportsVMailboxLookup for SQLiteDB {
    fn write_vmailbox_lookup_config_file(&self, output_path: &Path, mailbox_base_dir: &str) -> Result<(), Error> {
        let filename = output_path.file_name()
            .unwrap_or(OsStr::new("<filename unspecified>"))
            .to_str()
            .unwrap_or("<filename unspecified>");

        // Populate template with data
        let template = SQLiteVirtualMailboxLookupConfigTemplate {
            filename,
            generation_time: Local::now().to_string(),
            abs_db_path: self.make_absolute_db_path()?,
            mailbox_base_dir: String::from(mailbox_base_dir)
        };

        // Ensure the output directory we're supposed to be writing to exists
        let output_dir = output_path.parent().ok_or(Error::InvalidOrMissingConfig("failed to decipher vmailbox lookup config file output dir"))?;
        if !output_dir.exists() { create_dir_all(&output_dir)?; }

        // Generate paths for configuration files
        let path_str = output_path.to_str().unwrap_or("<filename missing>");
        debug!("writing vmailbox lookup config file @ [{:?}]", path_str);

        // Render the file contents, write them to disk
        let mut query_config_file = File::create(output_path)?;
        query_config_file.write_all(template.render()?.as_bytes())?;

        Ok(())
    }
}

// FIXME: mailbox_users table name is hardcoded here
const DEFAULT_USERDB_QUERY: &'static str = "SELECT home \
FROM mailbox_users \
WHERE username = '%n' AND domain = '%d'
";

// FIXME: mailbox_users table name is hardcoded here
const DEFAULT_PASSDB_QUERY: &'static str = "SELECT username, domain, password \
FROM mailbox_users \
WHERE username = '%n' AND domain = '%d'
";

// FIXME: mailbox_users table is hardcoded here
const DEFAULT_ITERATE_QUERY: &'static str = "SELECT username, domain FROM mailbox_users";

impl SupportsDovecotAuth for SQLiteDB {
    fn dovecot_userdb_settings(&self) -> Result<DovecotDBSettings, Error> {
        // Generate the DB settings for dovecot based on this backend
        Ok(DovecotDBSettings {
            driver: self.dovecot_driver_type()?,
            connect: self.cfg.path.clone(),
            query: String::from(DEFAULT_USERDB_QUERY),
            iterate_query: String::from(DEFAULT_ITERATE_QUERY),
        })
    }

    fn dovecot_passdb_settings(&self) -> Result<DovecotDBSettings, Error> {
        // Generate the DB settings for dovecot based on this backend
        Ok(DovecotDBSettings {
            driver: self.dovecot_driver_type()?,
            connect: self.cfg.path.clone(),
            query: String::from(DEFAULT_PASSDB_QUERY),
            iterate_query: String::from(DEFAULT_ITERATE_QUERY),
        })
    }

    fn dovecot_driver_type(&self) -> Result<DBType, Error> { Ok(DBType::SQLite) }
}

#[cfg(test)]
mod tests {
    use config::SQLiteDBCfg;
    use components::db::Connectable;
    use super::{SQLiteDB, MailboxDB, AdminDB};

    #[test]
    fn sqlite_db_starts_with_empty_conn() {
        let db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        assert!(db.conn.is_none(), "is none");
    }

    #[test]
    fn sqlite_db_init_mailbox_works() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        assert!(db.init_mailbox_db().is_ok());
    }

    #[test]
    fn sqlite_db_init_admin_works() {
        let mut db = SQLiteDB::new(SQLiteDBCfg::in_memory());
        let _ = db.connect();

        assert!(db.init_admin_db().is_ok());
        assert!(db.connection().is_ok());
    }
}
