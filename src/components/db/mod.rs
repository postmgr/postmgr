pub mod sqlite;

use components::Error;
use config::{DBCfg, DBType, DovecotDBSettings};
use models::user::MailboxUser;
use models::{ModelWithUUID, PaginatedList, PaginationOptions};
use self::sqlite::SQLiteDB;
use std::path::Path;
use rusqlite::Connection as SQLiteConnection;

pub enum DB {
    SQLite(SQLiteDB),
    PostgreSQL
}

impl DB {
    pub fn new(db_cfg: DBCfg) -> Result<DB, Error> where {
        match db_cfg.backend {
            DBType::SQLite => {
                let sqlite_cfg = db_cfg.sqlite.ok_or(Error::InvalidOrMissingConfig("sqlite db config missing"))?;
                Ok(DB::SQLite(SQLiteDB::new(sqlite_cfg)))
            }
            _ => Err(Error::NotSupported)
        }
    }
}

pub enum DBConnection {
    SQLite(SQLiteConnection),
    PostgreSQLConnection
}

pub trait Connectable {
    // Connect to the relevant database
    fn connect(&mut self) -> Result<(), Error>;

    // Ensure the Retrieve the DB connection
    fn connection(&self) -> Result<&DBConnection, Error>;
}

impl Connectable for DB {
    fn connect(&mut self) -> Result<(), Error> {
        match self {
            DB::SQLite(db) => db.connect(),
            _ => Err(Error::NotSupported)
        }
    }

    fn connection(&self) -> Result<&DBConnection, Error> {
        match self {
            DB::SQLite(db) => db.connection(),
            _ => Err(Error::NotSupported)
        }
    }
}

pub trait Versioned<T> {
    fn retrieve_version(&self) -> Result<T, Error>;
    fn persist_version(&self, v: T) -> Result<T, Error>;
}

pub trait Migratable<T> {
    fn run_migration(&self, m: &Migration) -> Result<(), Error>;
    fn migrate_to_latest(&self, migrations: &[Migration]) -> Result<T, Error>;
}

pub fn make_admin_db(db_cfg: DBCfg) -> Result<Box<AdminDB>, Error> {
    match db_cfg.backend {
        DBType::SQLite => {
            let sqlite_cfg = db_cfg.sqlite.ok_or(Error::InvalidOrMissingConfig("sqlite config for db not provided"))?;
            Ok(Box::new(SQLiteDB::new(sqlite_cfg)))
        },
        _ => Err(Error::NotSupported)
    }
}

pub trait MailboxDB
where
      Self: Connectable + SupportsVAliasLookup + SupportsVMailboxLookup
{
    // Initialize the database connection (if necessary), idempotently performing
    // all early/pre-work data changes and migrations necessary for a Postfix data holding db
    // Note that this *can* be the same DB as the Admin DB
    fn init_mailbox_db(&self) -> Result<(), Error>;

    fn add_mailbox_user(&self, u: MailboxUser) -> Result<ModelWithUUID<MailboxUser>, Error>;

    fn list_mailbox_users(&self, pagination: &PaginationOptions) -> Result<PaginatedList<ModelWithUUID<MailboxUser>>, Error>;

    fn get_mailbox_users_by_uuids(&self, uuids: &[String]) -> Result<Vec<ModelWithUUID<MailboxUser>>, Error>;

    fn remove_mailbox_user_by_username(&self, username: String) -> Result<(), Error>;
}

impl MailboxDB for DB {
    fn init_mailbox_db(&self) -> Result<(), Error> {
        match self {
            DB::SQLite(db) => db.init_mailbox_db(),
            _ => Err(Error::NotSupported)
        }
    }

    fn add_mailbox_user(&self, u: MailboxUser) -> Result<ModelWithUUID<MailboxUser>, Error> {
        match self {
            DB::SQLite(db) => db.add_mailbox_user(u),
            _ => Err(Error::NotSupported)
        }
    }

    fn list_mailbox_users(&self, pagination: &PaginationOptions) -> Result<PaginatedList<ModelWithUUID<MailboxUser>>, Error> {
        match self {
            DB::SQLite(db) => db.list_mailbox_users(pagination),
            _ => Err(Error::NotSupported)
        }
    }

    fn get_mailbox_users_by_uuids(&self, uuids: &[String]) -> Result<Vec<ModelWithUUID<MailboxUser>>, Error> {
        match self {
            DB::SQLite(db) => db.get_mailbox_users_by_uuids(uuids),
            _ => Err(Error::NotSupported)
        }
    }

    fn remove_mailbox_user_by_username(&self, username: String) -> Result<(), Error> {
        match self {
            DB::SQLite(db) => db.remove_mailbox_user_by_username(username),
            _ => Err(Error::NotSupported)
        }
    }
}

pub trait AdminDB {
    // Initialize the database connection (if necessary), idempotently performing
    // all early/pre-work data changes and migrations necessary for a Admin data holding db
    // Note that this *can* be the same DB as the Postfix DB
    fn init_admin_db(&self) -> Result<(), Error>;
}

// Forward-only SQL migrations
pub struct HardCodedSQLMigration {
    version: u32,
    sql: &'static str,
}

pub enum Migration {
    HardCodedSQL(HardCodedSQLMigration),
}

impl From<HardCodedSQLMigration> for Migration {
    fn from(m: HardCodedSQLMigration) -> Migration { Migration::HardCodedSQL(m) }
}

trait SQLMigration {
    fn version(&self) -> u32;
    fn sql(&self) -> String;
}

impl SQLMigration for HardCodedSQLMigration {
    fn version(&self) -> u32 { self.version }
    fn sql(&self) -> String { String::from(self.sql) }
}

impl SQLMigration for Migration {
    fn version(&self) -> u32 {
        match self {
            Migration::HardCodedSQL(m) => m.version(),
        }
    }

    fn sql(&self) -> String {
        match self {
            Migration::HardCodedSQL(m) => m.sql(),
        }
    }
}

pub trait SupportsVAliasLookup {
    // Write out the file that postfix will use as configuration for performing virtual alias lookups
    fn write_valias_lookup_config_file(&self, output_path: &Path) -> Result<(), Error>;
}

impl SupportsVAliasLookup for DB {
    fn write_valias_lookup_config_file(&self, output_path: &Path) -> Result<(), Error> {
        match self {
            DB::SQLite(db) => db.write_valias_lookup_config_file(output_path),
            _ => Err(Error::NotSupported)
        }
    }
}

pub trait SupportsVMailboxLookup {
    // Write out the file that postfix will use as configuration for performing virtual mailbox lookups
    fn write_vmailbox_lookup_config_file(&self, output_path: &Path, mailbox_base_dir: &str) -> Result<(), Error>;
}

impl SupportsVMailboxLookup for DB {
    fn write_vmailbox_lookup_config_file(&self, output_path: &Path, mailbox_base_dir: &str) -> Result<(), Error> {
        match self {
            DB::SQLite(db) => db.write_vmailbox_lookup_config_file(output_path, mailbox_base_dir),
            _ => Err(Error::NotSupported)
        }
    }
}

// TODO: this can probably be generalized in the future to SupportsAuth<T> when Dovecot auth is added
pub trait SupportsDovecotAuth where Self: MailboxDB {
    /// Dovecot userdb settings
    fn dovecot_userdb_settings(&self) -> Result<DovecotDBSettings, Error>;

    /// Dovecot passdb settings
    fn dovecot_passdb_settings(&self) -> Result<DovecotDBSettings, Error>;

    /// Get the driver for dovecot
    fn dovecot_driver_type(&self) -> Result<DBType, Error>;
}

impl SupportsDovecotAuth for DB {
    fn dovecot_userdb_settings(&self) -> Result<DovecotDBSettings, Error> {
        match self {
            DB::SQLite(db) => db.dovecot_userdb_settings(),
            _ => Err(Error::NotSupported),
        }
    }

    fn dovecot_passdb_settings(&self) -> Result<DovecotDBSettings, Error> {
        match self {
            DB::SQLite(db) => db.dovecot_passdb_settings(),
            _ => Err(Error::NotSupported),
        }
    }

    fn dovecot_driver_type(&self) -> Result<DBType, Error> {
        match self {
            DB::SQLite(db) => db.dovecot_driver_type(),
            _ => Err(Error::NotSupported),
        }
    }
}
