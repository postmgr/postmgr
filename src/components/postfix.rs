use std::fs::{create_dir_all, File, copy};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio, Child as ChildProcess};
use std::sync::mpsc::{sync_channel, SyncSender, Receiver};

use askama::Template;
use chrono::prelude::Local;
use make_absolute_path;
use simple_signal::{Signal};
use simple_signal;

use components::db::*;
use components::*;
use config::{PostfixCfg, MilterCfg, SMTPTLSCfg, SMTPDTLSCfg, DBCfg, DBType};
use models::user::MailboxUser;

pub struct Postfix {
    cfg: PostfixCfg,

    cmd_bus_rx: Receiver<ReqResp<PostfixCmd>>,
    cmd_bus_tx: SyncSender<ReqResp<PostfixCmd>>,

    pid: Option<u32>,
    command: Option<Command>,
    process: Option<ChildProcess>,

    pub db: DB,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum PostfixCmd {
    Lifecycle(ComponentCmd),

    // user management
    CreateUser(MailboxUser),
}

impl Postfix {
    pub fn new(cfg: PostfixCfg, db: Option<DB>) -> Result<Postfix, Error> {
        // Create (if necessary) and connect DB
        let mut db = db.unwrap_or(DB::new(cfg.db.clone())?);
        db.connect()?;

        // Create command bus channels
        let (cmd_bus_tx, cmd_bus_rx) = sync_channel(0);

        Ok(Postfix {
            cfg,
            cmd_bus_tx,
            cmd_bus_rx,
            pid: None,
            command: None,
            process: None,
            db: db,
        })
    }

    pub fn mail_directory(&self) -> String {
        self.cfg.mail_spool_directory.clone()
    }

}

impl Component for Postfix {
    fn get_name(&self) -> &str { "Postfix" }

    fn start(&mut self) -> Result<(), Error> {
        if let Some(_) = self.process { return Err(Error::AlreadyRunningError); }

        self.spawn_child_process()?;

        // Control loop, this never exits
        for req in self.cmd_bus_rx.iter() {
            match req.msg {
                PostfixCmd::Lifecycle(ComponentCmd::Shutdown) => {
                    debug!("received shutdown command to component");
                    break;
                },

                PostfixCmd::CreateUser(new_user) => {
                    debug!("creating new mailbox user with email [{}]", &new_user.username);
                    // Attempt to add the new user
                    let result = self.db.add_mailbox_user(new_user);

                    match result {
                        Ok(new_user) => {
                            debug!("successfully created new user with email: [{}]", &new_user.model.username);
                            if let Some(chan) = req.response_chan {
                                chan.send(Box::new(new_user)).unwrap();
                            }
                        },
                        Err(err) => {
                            error!("error creating new user: {:?}", err);
                            if let Some(chan) = req.response_chan {
                                chan.send(Box::new(err)).unwrap();
                            }
                        },
                    }

                },
            }
        }

        // Stop the child process since we've left the control loop
        self.stop()?;

        Ok(())
    }

    fn stop(&mut self) -> Result<(), Error> {
        if self.process.is_none() { return Err(Error::NotRunningError); }

        // assuming that if a process is present a PID is present
        let pid = self.pid.unwrap();
        debug!("killing postfix process (PID {})...", &pid);

        if let Some(ref mut p) = self.process {
            // TODO: Figure out a better way to do this that doesn't use the binary directly
            // would be nice to have multiple postmgr instances running at one time without
            // manpages say sending SIGTERM would do but rust can't handle signals yet...
            // https://github.com/rust-lang/rfcs/issues/1368
            let stop_output = Command::new(&self.cfg.bin_path)
                .arg("stop")
                .output()
                .expect("failed to run postfix stop");

            if !stop_output.status.success() {
                debug!("`postfi stop` failed, sending SIGTERM and killing the process from rust");
                Command::new("kill")
                    .arg("-TERM")
                    .arg(&pid.to_string())
                    .output()
                    .expect("failed to send kill signal to postfix");

                p.kill()?;
            }

            // Wait for the process to exit completely
            p.wait()?;
        }

        return Ok(());
    }

}

impl HasReqRespCommandBus for Postfix {
    type Msg = PostfixCmd;

    fn get_cmd_bus_tx(&self) -> Result<SyncSender<ReqResp<Self::Msg>>, Error> {
        Ok(self.cmd_bus_tx.clone())
    }
}

impl ChildProcessManager for Postfix {
    fn spawn_child_process(&mut self) -> Result<&Option<ChildProcess>, Error> {
        debug!("spawning postfix process...");

        // Build arguments
        let config_output_dir = make_absolute_path(PathBuf::from(&self.cfg.config_output_dir))?;
        let args = &[
            "-c",
            config_output_dir.as_str(),
            "-v",
            "start",
        ] ;

        // Build & save command
        let mut cmd = Command::new(&self.cfg.bin_path);
        // Config directory for daemons must be specified with ENV
        // http://www.postfix.org/postconf.5.html#config_directory
        cmd.env("MAIL_CONFIG", &config_output_dir);
        cmd.args(args);
        cmd.stdin(Stdio::null());
        cmd.stdout(Stdio::inherit());
        debug!("command: [{:#?}]!", cmd);
        self.command = Some(cmd);

        // Actually run the command
        // NOTE: unfortunately we have to build it all over again since Command can't be cloned
        let child = Command::new(&self.cfg.bin_path)
            .args(args)
            .env("MAIL_CONFIG", &config_output_dir)
            .stdin(Stdio::null())
            //.stdout(Stdio::inherit())
            .spawn()?;
        self.pid = Some(child.id());
        self.process = Some(child);

        // Setup signal handlers with a channel
        let postfix_tx = self.get_cmd_bus_tx()?;
        simple_signal::set_handler(&[Signal::Int, Signal::Term], move |_signals| {
            info!("Received SIGINT/SIGTERM, forwarding to all components");
            postfix_tx.send(ReqResp{
                msg: PostfixCmd::Lifecycle(ComponentCmd::Shutdown),
                response_chan: None
            }).expect("failed to send shutdown command to postfix component");
        });

        Ok(&self.process)
    }

    fn get_pid(&self) -> Option<u32> { self.pid }
    fn get_command(&self) -> &Option<Command> { &self.command }
    fn get_process(&self) -> &Option<ChildProcess> { &self.process }
}

impl Configurable<PostfixCfg> for Postfix {
    fn update_config(&mut self, _cfg: Option<PostfixCfg>) -> Result<(), Error> {
        info!("updating postfix configuration...");
        self.generate_config_in_dir(None)?;
        self.install_config()?;
        Ok(())
    }

    fn get_config(&self) -> &PostfixCfg { &self.cfg }
}

#[derive(Template)]
#[template(path = "config/postfix/main.cf.jinja")]
struct PostfixMainCFTemplate<'a> {
    filename: &'a str,
    generation_time: String,

    sendmail_bin_path: &'a String,
    mailq_bin_path: &'a String,
    newaliases_bin_path: &'a String,

    local_hostname: &'a String,
    internet_hostname: &'a String,
    default_domain_name: &'a String,

    inbound_domain_names: &'a Vec<String>,
    allowed_relay_cidrs: &'a Vec<String>,
    allowed_relay_destinations: &'a Vec<String>,
    virtual_mailbox_domains: &'a Vec<String>,

    delivery_relay_host: &'a Option<String>,

    recipient_delimiter: &'a String,

    submission_qmgr_group_name: &'a String,

    proxy_address: &'a Option<String>,
    postmaster_notification_classes: &'a Vec<String>,
    mail_spool_directory: &'a String,
    message_size_limit_bytes: i32,
    mailbox_size_limit_bytes: &'a Option<i32>,

    milters: &'a Option<MilterCfg>,

    smtp_tls: &'a Option<SMTPTLSCfg>,
    smtpd_tls: &'a Option<SMTPDTLSCfg>,

    mailbox_base_dir: &'a String,

    valias_lookup_cfg_file_path: String,
    vmailbox_lookup_cfg_file_path: String,

    smtpd_sasl_type: String,
    smtpd_sasl_path: String,

    db: &'a DBCfg
}

#[derive(Template)]
#[template(path = "config/postfix/master.cf.jinja")]
struct PostfixMasterCFTemplate<'a> {
    filename: &'a str,
    generation_time: String,
}

impl FileConfigurable<PostfixCfg> for Postfix {
    fn generate_config_in_dir(&self, dir: Option<String>) -> Result<(), Error> {
        info!("updating on-disk configuration for postfix");

        // Generate file paths for the virtual aliasing lookup config files the DB *may* generate
        let valias_lookup_cfg_path = Path::new(&self.cfg.config_dir).join("valias_lookup.cf");
        let valias_lookup_cfg_file_path = make_absolute_path(valias_lookup_cfg_path.to_path_buf())?;

        // Generate file paths for the virtual mailbox lookup config files the DB *may* generate
        let vmailbox_lookup_cfg_path = Path::new(&self.cfg.config_dir).join("vmailbox_lookup.cf");
        let vmailbox_lookup_cfg_file_path = make_absolute_path(vmailbox_lookup_cfg_path.to_path_buf())?;

        let main_cf = PostfixMainCFTemplate {
            filename: "main.cf",
            generation_time: Local::now().to_string(),

            sendmail_bin_path: &self.cfg.sendmail_bin_path,
            mailq_bin_path: &self.cfg.mailq_bin_path,
            newaliases_bin_path: &self.cfg.newaliases_bin_path,

            internet_hostname: &self.cfg.internet_hostname,
            local_hostname: &self.cfg.local_hostname,
            default_domain_name: &self.cfg.default_domain_name,
            inbound_domain_names: &self.cfg.inbound_domain_names,
            allowed_relay_cidrs: &self.cfg.allowed_relay_cidrs,
            allowed_relay_destinations: &self.cfg.allowed_relay_destinations,

            delivery_relay_host: &self.cfg.delivery_relay_host,
            postmaster_notification_classes: &self.cfg.postmaster_notification_classes,
            proxy_address: &self.cfg.proxy_address,
            recipient_delimiter: &self.cfg.recipient_delimiter,
            mail_spool_directory: &self.cfg.mail_spool_directory,
            message_size_limit_bytes: self.cfg.message_size_limit_bytes,
            mailbox_size_limit_bytes: &self.cfg.mailbox_size_limit_bytes,

            submission_qmgr_group_name: &self.cfg.submission_qmgr_group_name,

            smtp_tls: &self.cfg.smtp_tls,
            smtpd_tls: &self.cfg.smtpd_tls,
            milters: &self.cfg.milters,

            virtual_mailbox_domains: &self.cfg.virtual_mailbox_domains,
            mailbox_base_dir: &self.cfg.mail_spool_directory,

            valias_lookup_cfg_file_path,
            vmailbox_lookup_cfg_file_path,

            // FIXME: should be generated/determined based on postfix cfg
            // Another trait? Box up these config options into some sort of SASLCfg?
            smtpd_sasl_type: String::from("dovecot"),
            smtpd_sasl_path: String::from("private/auth"),

            db: &self.cfg.db
        };

        // Generate master.cf
        let master_cf = PostfixMasterCFTemplate {
            filename: "master.cf",
            generation_time: Local::now().to_string(),
        };

        // Generate DB-specific query instruction file for vmailbox and valias mappings
        debug!("writing db-specific valias and vmailbox lookup files...");
        self.db.write_valias_lookup_config_file(valias_lookup_cfg_path.as_path())?;
        self.db.write_vmailbox_lookup_config_file(vmailbox_lookup_cfg_path.as_path(), &self.cfg.mail_spool_directory)?;

        // Generate the directories the files are going to go into
        let config_dir = dir.unwrap_or(self.config_dir_path());
        let config_dir_path = Path::new(&config_dir);

        // Ensure config directory exists and is a proper directory
        debug!("writing config files to dir [{:?}]", config_dir_path.to_str());
        if !config_dir_path.exists() { create_dir_all(&config_dir_path)?; }
        if config_dir_path.exists() && !config_dir_path.is_dir() {
            return Err(Error::InvalidEnvironment("Provided config dir is not a directory"));
        }

        // Ensure the mail data base directory (which whill hold all the data) exists
        let mailbox_base_dir = &self.cfg.mail_spool_directory;
        let mailbox_base_dir_path = Path::new(&mailbox_base_dir);
        if !mailbox_base_dir_path.exists() { create_dir_all(&mailbox_base_dir_path)?; }

        // Generate paths for configuration files
        let main_cf_path = config_dir_path.join(main_cf.filename);
        let master_cf_path = config_dir_path.join(master_cf.filename);

        // Render the file contents
        debug!("main.cf will be written to [{:?}]", main_cf_path.to_str());
        debug!("master.cf will be written to [{:?}]", master_cf_path.to_str());
        let mut main_cf_file = File::create(main_cf_path)?;
        let mut master_cf_file = File::create(master_cf_path)?;

        main_cf_file.write_all(main_cf.render()?.as_bytes())?;

        master_cf_file.write_all(master_cf.render()?.as_bytes())?;

        debug!("successfully wrote configuration files to disk");
        Ok(())
    }

    fn install_config(&self) -> Result<(), Error> {
        // Generate the directories the files are going to go into
        let config_dir_path = Path::new(&self.cfg.config_dir);
        let config_output_dir_path = Path::new(&self.cfg.config_output_dir);
        debug!(
            "installing config file(s) from directory [{:?}] to [{:?}]",
            config_dir_path.to_str(),
            config_output_dir_path.to_str(),
        );

        // Ensure config output path exists
        debug!("ensuring configuration output path [{:?}] exists...", config_output_dir_path);
        if !config_output_dir_path.exists() { create_dir_all(&config_output_dir_path)?; }

        // Copy main.cf
        let main_cf_from = config_dir_path.clone().join("main.cf");
        let main_cf_to = config_output_dir_path.clone().join("main.cf");
        debug!("copying main.cf from [{:?}] to [{:?}]", main_cf_from, main_cf_to);
        copy(&main_cf_from, &main_cf_to)?;

        // Copy master.cf
        let master_cf_from = config_dir_path.clone().join("master.cf");
        let master_cf_to = config_output_dir_path.clone().join("master.cf");
        debug!("copying master.cf from [{:?}] to [{:?}]", master_cf_from, master_cf_to);
        copy(&master_cf_from, &master_cf_to)?;

        Ok(())
    }

    fn config_dir_path(&self) -> String { self.cfg.config_dir.clone() }
}
