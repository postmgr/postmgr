#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MailboxUser {
    pub username: String,
    pub domain: String,
    pub home: String,
    pub password: String,
    pub quota_gb: i32
}

impl MailboxUser {
    pub fn new(domain: String, home: String, username: String, password: String) -> MailboxUser {
        MailboxUser {
            username,
            domain,
            password,
            home,
            quota_gb: 0
        }
    }
}
