pub mod user;

use uuid::Uuid;
use components::Error;
use std::marker::Sized;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ModelWithUUID<T> {
    pub uuid: String,
    pub model: T
}

impl<T> ModelWithUUID<T> {
    pub fn from_model(model: T) -> ModelWithUUID<T> {
        ModelWithUUID {
            uuid: Uuid::new_v4().to_string(),
            model
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PaginationOptions {
    limit: Option<u32>,
    offset: Option<u32>,
    sorting: Option<Sorting>
}

const DEFAULT_LIMIT: u32 = 25;
const DEFAULT_OFFSET: u32 = 0;

impl PaginationOptions {
    pub fn new() -> PaginationOptions {
        PaginationOptions {
            limit: Some(DEFAULT_LIMIT),
            offset: Some(DEFAULT_OFFSET),
            sorting: Some(Sorting::Default)
        }
    }

    pub fn get_limit_or_default(&self) -> u32 { self.limit.unwrap_or(DEFAULT_LIMIT) }
    pub fn get_offset_or_default(&self) -> u32 { self.offset.unwrap_or(DEFAULT_OFFSET) }
}

#[derive(Debug, Deserialize, Serialize)]
pub enum Sorting {
    Default,
    ColumnAndOrder(String, SortOrder)
}

#[derive(Debug, Deserialize, Serialize)]
pub enum SortOrder {
    ASC,
    DESc
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PaginatedList<T> {
    pub items: Vec<T>,
    pub total: u32
}

// Enable database specific code to perform operations against a given object (usually a model)
pub trait DBEntity<D, R> where Self: Sized {
    // Generate the DB entity from a "row" (normally a result of a database query)
    fn from_row(row: &R) -> Result<Self, Error>;

    // Insert the entity into a given database
    fn insert(&self, db: &D) -> Result<&Self, Error>;

    // List the entities in the database
    fn list(db: &D, pagination: &PaginationOptions) -> Result<PaginatedList<Self>, Error>;

    // Delete the entity from the database
    fn delete(&self, db: &D) -> Result<(), Error>;

    // Delete an entity with the given UUID from the database
    fn delete_by_uuid(db: &D, uuid: &String) -> Result<(), Error>;

    // Retrieve entities with the given UUIDs from the database
    fn find_by_uuids(db: &D, uuids: &[String]) -> Result<Vec<Self>, Error>;

    // Retrieve entities with the given textual field set to the specified textual value
    fn find_by_text_field(db: &D, field: &str, value: String) -> Result<Vec<Self>, Error>;
}

pub trait HasSQLTable<D> {
    fn sql_tbl_name() -> &'static str;
    fn sql_tbl_columns() -> &'static [&'static str];
}
